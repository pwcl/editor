﻿Shader "Unlit/TransparentUnlit" {
    Properties{
        _ColorTexture("Color Texture", 2D) = "white" {}
        _OverlayColor("Overlay Color", Color) = (1, 1, 1, 1)
        _Color("Color", Color) = (1, 1, 1, 1)
    }
        SubShader{
            Tags { "RenderType" = "Opaque" "Queue" = "Transparent" }
            LOD 100
            Blend SrcAlpha OneMinusSrcAlpha

            Pass {
                CGPROGRAM
                    #pragma vertex vert
                    #pragma fragment frag
                    #pragma target 2.0
                    #include "UnityCG.cginc"

                    struct appdata_t {
                        float4 vertex : POSITION;
                        UNITY_VERTEX_INPUT_INSTANCE_ID
                    };

                    struct v2f {
                        float4 vertex : SV_POSITION;
                        float4 screenPos : TEXCOORD0;
                        UNITY_VERTEX_OUTPUT_STEREO
                    };

                    float4 _Color;
                    sampler2D _ColorTexture;
                    float4 _ColorTexture_ST;

                    v2f vert(appdata_t v)
                    {
                        v2f o;
                        UNITY_SETUP_INSTANCE_ID(v);
                        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                        o.vertex = UnityObjectToClipPos(v.vertex);
                        o.screenPos = ComputeScreenPos(o.vertex);
                        UNITY_TRANSFER_FOG(o, o.vertex);
                        return o;
                    }

                    fixed4 frag(v2f i) : SV_Target
                    {
                        fixed4 col = _Color;
                        UNITY_APPLY_FOG(i.fogCoord, col);
                        return col;
                    }
                ENDCG
            }

            Pass {
                CGPROGRAM
                    #pragma vertex vert
                    #pragma fragment frag
                    #pragma target 2.0
                    #include "UnityCG.cginc"

                    struct appdata_t {
                        float4 vertex : POSITION;
                        UNITY_VERTEX_INPUT_INSTANCE_ID
                    };

                    struct v2f {
                        float4 vertex : SV_POSITION;
                        float4 screenPos : TEXCOORD0;
                        UNITY_VERTEX_OUTPUT_STEREO
                    };

                    float4 _OverlayColor;
                    sampler2D _ColorTexture;
                    float4 _ColorTexture_ST;

                    v2f vert(appdata_t v)
                    {
                        v2f o;
                        UNITY_SETUP_INSTANCE_ID(v);
                        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                        o.vertex = UnityObjectToClipPos(v.vertex);
                        o.screenPos = ComputeScreenPos(o.vertex);
                        UNITY_TRANSFER_FOG(o, o.vertex);
                        return o;
                    }

                    fixed4 frag(v2f i) : SV_Target
                    {
                        fixed4 col = _OverlayColor;
                        col.a = 0.5;
                        UNITY_APPLY_FOG(i.fogCoord, col);
                        return col;
                    }
                ENDCG
            }
        }
}
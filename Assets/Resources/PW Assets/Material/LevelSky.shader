﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "PistolWhip/LevelSky" {
	Properties{
		_FogColor("Fog Color", Color) = (0.5, 0.5, 0.5, 1)
	}

		SubShader{
			Tags { "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }
			Cull Off ZWrite Off

			Pass {

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"
				#include "Lighting.cginc"

				float4 _FogColor;

				struct appdata_t
				{
					float4 vertex : POSITION;
				};

				struct v2f
				{
					float4  pos : SV_POSITION;
				};

				v2f vert(appdata_t v)
				{
					v2f o;
					o.pos = round(v.vertex);
					return o;
				}

				float4 frag(v2f IN) : SV_Target
				{
					return _FogColor;
				}
					ENDCG
			}
	}
}

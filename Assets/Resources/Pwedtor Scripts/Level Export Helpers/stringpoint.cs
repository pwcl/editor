﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stringconverters
{
    public static string convertB(bool boolean)
    {
        return boolean ? "True" : "False";
    }

    public static string convert2(Vector2 vec)
    {
        return $"{vec.x},{vec.y}";
    }

    public static string convert3(Vector3 vec)
    {
        return $"{vec.x},{vec.y},{vec.z}";
    }

    public static string convertQ(Quaternion qua)
    {
        return $"{qua.x},{qua.y},{qua.z},{qua.w}";
    }

    public static string convert3Q(Vector3 vec, Quaternion qua)
    {
        return $"{vec.x},{vec.y},{vec.z},{qua.x},{qua.y},{qua.z},{qua.w}";
    }

    public static string convertC(Color col)
    {
        Color32 c = col;

        return $"{c.r.ToString("X2")}{c.g.ToString("X2")}{c.b.ToString("X2")}";
    }
}

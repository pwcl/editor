﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class LevelFile
{
    public float version;
    public string sceneDisplayName;
    public string songDisplayName;
    public string songLocation;
    public float songLength;
    public string description;
    public string songArtist;
    public string mapper;
    public float tempo;
    public float previewTime;
    public List<string> maps = new List<string>(0);
    public LevelInfo.ObstacleSets obstacleSet;
    public LevelInfo.MaterialPropertiesSet materialPropertiesSet;
    public LevelInfo.EnemySets enemySet;
    public float moveSpeed;
    public float timingWindowSize;
    public menuColor customMainMenuColor;

    public class menuColor
    {
        public Color Main;
        public Color Fog;
        public Color Glow;
        public Color Enemy;

        public menuColor(Color m, Color f, Color g, Color e)
        {
            Main = m;
            Fog = f;
            Glow = g;
            Enemy = e;
        }
    }

    public static LevelFile Generate(LevelInfo levelInfo, float ver)
    {
        LevelFile levelFile = new LevelFile();
        levelFile.version = ver;
        levelFile.sceneDisplayName = levelInfo.songName + " ~ " + levelInfo.songArtists + "~" + levelInfo.mapper;
        levelFile.songDisplayName = levelInfo.songName;
        levelFile.songLocation = EditorManager.Instance.soundManager.clipPath;
        levelFile.songLength = levelInfo.songLength;
        levelFile.description = levelInfo.description;
        levelFile.songArtist = levelInfo.songArtists;
        levelFile.mapper = levelInfo.mapper;
        levelFile.tempo = levelInfo.bpm;
        levelFile.previewTime = levelInfo.previewTime;

        if (levelInfo.hasMaps[0]) { levelFile.maps.Add("Easy"); }
        if (levelInfo.hasMaps[1]) { levelFile.maps.Add("Normal"); }
        if (levelInfo.hasMaps[2]) { levelFile.maps.Add("Hard"); }

        levelFile.obstacleSet = levelInfo.obstacleStyle;
        levelFile.materialPropertiesSet = levelInfo.materialStyle;
        levelFile.enemySet = levelInfo.enemyStyle;
        levelFile.moveSpeed = levelInfo.moveSpeed;
        levelFile.timingWindowSize = levelInfo.timingWindowSize;
        levelFile.customMainMenuColor = new menuColor(levelInfo.mainColor, levelInfo.fogColor, levelInfo.glowColor, levelInfo.enemyColor);

        return levelFile;
    }
}

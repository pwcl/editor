﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class SaveManager : UnityEditor.AssetModificationProcessor
{
    public static float EDITOR_VERSION = 0.5f;
    public static string[] OnWillSaveAssets(string[] paths)
    {
        // Get the name of the scene to save.
        string scenePath = string.Empty;
        string sceneName = string.Empty;

        foreach (string path in paths)
        {
            if (path.Contains(".unity"))
            {
                scenePath = Path.GetDirectoryName(path);
                sceneName = Path.GetFileNameWithoutExtension(path);
            }
        }

        if (sceneName.Length == 0)
        {
            return paths;
        }

        if (!Directory.Exists("Saved Data")) Directory.CreateDirectory("Saved Data");
        if (!Directory.Exists("Saved Data/Easy")) Directory.CreateDirectory("Saved Data/Easy");
        if (!Directory.Exists("Saved Data/Normal")) Directory.CreateDirectory("Saved Data/Normal");
        if (!Directory.Exists("Saved Data/Hard")) Directory.CreateDirectory("Saved Data/Hard");

        GameObject manager = GameObject.Find("Editor Manager");

        if (manager != null)
        {
            manager.GetComponent<EditorManager>().Save();
            manager.transform.GetComponentInChildren<EnemyManager>().Save();

        }

        return paths;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor.SceneManagement;
using Newtonsoft.Json.Converters;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Newtonsoft.Json;

[ExecuteInEditMode]
public class EnemyManager : MonoBehaviour
{
    public List<Enemy> enemies = new List<Enemy>(0);
    int enemyStyle = 0;
    float savedTime = 0;
    [SerializeField]
    public int EnemyStyle
    {
        get { 
            return enemyStyle; 
        }

        set { 
            enemyStyle = value;

            foreach (Enemy enemy in enemies)
            {
                enemy.UpdateStyle(value);
            }
        }
    }
    int enemyToAnimate = 0;
    public static float trackHeight = 33;
    public EditorManager manager;

    void Awake()
    {
        Init();
    }

    void OnValidate()
    {
        Init();
    }

    void Init()
    {
        enemies.Clear();

        for (int i = 0; i < transform.childCount; i++)
        {
            enemies.Add(transform.GetChild(i).GetComponent<Enemy>());
            enemies[enemies.Count - 1].SetMaterials();
            enemies[enemies.Count - 1].UpdateStyle(enemyStyle);
        }

        manager = transform.parent.GetComponent<EditorManager>();
    }

    void Update()
    {
        
    }

    public void UpdateEnemies(float time, int style, Color currentEnemyColor)
    {
        if (style != EnemyStyle)
        {
            EnemyStyle = style;
        }

        List<Enemy> updatePool = new List<Enemy>(0);

        List<Enemy> removed = new List<Enemy>(0);

        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null && enemies[i].gameObject != null)
            {
                enemies[i].UpdateEnemy(time, currentEnemyColor);

                if (enemies[i].transform.GetChild(0).GetComponent<MeshRenderer>().isVisible && Vector3.Distance(SceneView.lastActiveSceneView.camera.transform.position, enemies[i].transform.position) <= 20)
                {
                    updatePool.Add(enemies[i]);
                }
            } else
            {
                removed.Add(enemies[i]);
            }
        }

        savedTime = time;

        bool cleaned = false;

        foreach (Enemy enemy in removed)
        {
            enemies.Remove(enemy);
            cleaned = true;
        }

        if (cleaned)
        {
            EditorUtility.SetDirty(this);
        }

        if (enemies.Count != transform.childCount)
        {
            enemies.Clear();

            for (int i = 0; i < transform.childCount; i++)
            {
                enemies.Add(transform.GetChild(i).GetComponent<Enemy>()); 
            }

            EditorUtility.SetDirty(this);
        }

        int animatePerFrame = Mathf.Min(3, updatePool.Count);

        //Debug.Log($"Enemy count in animation pool {updatePool.Count}, number animating per frame {animatePerFrame}");

        for (int i = 0; i < animatePerFrame; i++)
        {
            if (enemyToAnimate >= updatePool.Count)
            {
                enemyToAnimate = 0;
            }
            updatePool[enemyToAnimate].AnimateEnemy(time);
            enemyToAnimate++;
        }
    }

    public void Draw()
    {
        foreach (Enemy enemy in enemies)
        {
            
        }
    }

    public void Save()
    {
        string savePath = Path.Combine(new string[3] { "Saved Data", EditorSceneManager.GetActiveScene().name, "Enemies.json" });

        var jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new ColorConverter(),
                new VectorConverter(),
                new QuaternionConverter(),
                new StringEnumConverter()
            }
        };

        File.WriteAllText(savePath, JsonConvert.SerializeObject(new EnemyList(enemies), jsonSettings));
    }


    public void Load()
    {
        string savePath = Path.Combine(new string[3] { "Saved Data", EditorSceneManager.GetActiveScene().name, "Enemies.json" });

        if (File.Exists(savePath))
        {
            //levelInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(savePath));
        }
    }
}

public class EnemyList
{
    public List<Enemy> enemies = new List<Enemy>(0);

    public EnemyList(List<Enemy> allEnemies)
    {
        this.enemies = allEnemies;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using UnityEditor.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class EditorManager : MonoBehaviour
{
    public LevelInfo levelInfo = new LevelInfo();
    public LightingManager lightingManager;
    public SoundManager soundManager;
    public EnemyManager enemyManager;
    public static EditorManager Instance { get; private set; }

    public bool followAlong = false;
    public Vector2 maxScroll = Vector2.one;

    void Start()
    {
        Init();
    }

    void OnValidate()
    {
        EditorApplication.update -= UpdateManagers;
        Init();
    }

    void Init()
    {
        lightingManager = GetComponentInChildren<LightingManager>();
        soundManager = GetComponentInChildren<SoundManager>();
        enemyManager = GetComponentInChildren<EnemyManager>();

        Instance = this;
        EditorUtility.SetDirty(this);

        EditorApplication.update += UpdateManagers;
        EditorSceneManager.sceneOpening += SceneOpen;
        soundManager.UpdateSong(0, 2);
    }

    private void SceneOpen(string path, OpenSceneMode mode)
    {
        EditorApplication.update -= UpdateManagers;
    }

    public void UpdateManagers()
    {
        if (this == null)
        {
            EditorApplication.update -= UpdateManagers;
            return;
        }

        if (enemyManager == null)
        {
            enemyManager = GetComponentInChildren<EnemyManager>();
        }
        if (lightingManager == null)
        {
            lightingManager = GetComponentInChildren<LightingManager>();
        }

        enemyManager.UpdateEnemies(soundManager.songTime, (int)levelInfo.enemyStyle, lightingManager.currentEnemyColor);
        lightingManager.UpdateColors(soundManager.songTime);
    }

    public void UpdateSong(float time, int playState)
    {
        soundManager.UpdateSong(time, playState);
    }

    public AudioClip GetSong()
    {
        return soundManager.GetSong();
    }

    public float SongTime()
    {
        return soundManager.songTime;
    }

    public bool IsPlaying()
    {
        return soundManager.playing;
    }

    public void SetSong(AudioClip newSong)
    {
        soundManager.SetSong(newSong);
        soundManager.UpdateWaveform();
    }

    public void Save()
    {
        string savePath = Path.Combine("Saved Data", "Level Info.json");
        File.WriteAllText(savePath, levelInfo.ToString());

        /*
         * var levelInfo = new SerializedObject(GameObject.Find("Editor Manager").GetComponent<EditorManager>());
         * levelInfo.FindProperty("levelInfo").FindPropertyRelative("timingWindowSize").floatValue = 727;
         * levelInfo.ApplyModifiedProperties();
         */
    }


    public void Load()
    {
        string savePath = Path.Combine("Saved Data", "Level Info.json");
        if (File.Exists(savePath))
        {
            levelInfo = JsonConvert.DeserializeObject<LevelInfo>(File.ReadAllText(savePath));
        }
    }

    public static void ClearConsole()
    {
        var logEntries = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");
        var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
        clearMethod.Invoke(null, null);
    }
}

public enum Difficulty
{
    Easy = 0,
    Normal = 1,
    Hard = 2
}

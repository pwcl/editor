﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel;
using System.Reflection;
using Newtonsoft.Json.Converters;
using UnityEditor;

[JsonObject(MemberSerialization.OptIn)]
[Serializable]
[ExecuteInEditMode]
public class Enemy : MonoBehaviour
{
    [JsonProperty]
    public string Name = "";
    [JsonProperty]
    public List<Action> actions = new List<Action>(0);
    public float positionAlong = 0;
    [JsonProperty]
    public float time = 0;
    [JsonProperty]
    public bool shielded = false;
    [JsonProperty]
    public Distance distance = Distance.Near;
    [JsonProperty]
    public Vector2Int placement = Vector2Int.zero;
    [JsonProperty]
    public EnemyType type = EnemyType.Slot1;
    public EnemyType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
            UpdateStyle(enemyStyle);
        }
    }
    public int enemyStyle = 0;
    [JsonProperty]
    public Quaternion rotation = Quaternion.Euler(0, 180, 0);
    public Vector3 rotationBuffer;
    public Vector3 RotationBuffer
    {
        get
        {
            return rotationBuffer;
        }

        set
        {
            rotationBuffer = value;
            rotation = Quaternion.Euler(value);
        }
    }
    [JsonProperty]
    public WorldPoint offset = new WorldPoint(Vector3.zero, Quaternion.Euler(0, 180, 0));
    //[JsonProperty]
    //bool ignoreForLevelRank = false;
    [JsonProperty]
    public List<float> fireTimes = new List<float>(0);
    [JsonProperty]
    public CheevoID cheevoID = CheevoID.None;
    [JsonProperty]
    public bool noGround = false;
    [JsonProperty]
    public bool noCarve = false;
    [JsonProperty]
    public bool bonusEnemy = false;
    [JsonProperty]
    public Facing facing = Facing.Forward;
    [JsonProperty]
    public float duration = 0;
    [JsonProperty]
    public CheevoID forceCheevoID = CheevoID.None;
    [JsonProperty]
    public float playerActionLerp = 0;
    [JsonProperty]
    public WorldPoint localActionPoint = new WorldPoint(Vector3.zero, Quaternion.identity);
    [JsonProperty]
    public bool snapToDefault = true;
    public float despawn = 0;
    [JsonProperty]
    public int onTrack = 0;
    [JsonProperty]
    public Rect enemyRect = new Rect(new Vector2(0, 0), new Vector2(0, 0));
    public EnemyManager manager;


    bool enemyActive = false;
    AnimationClip clip;
    List<Material> enemyMats = new List<Material>(0);
    MeshRenderer[] renderers = new MeshRenderer[3];
    SkinnedMeshRenderer[] srenderers = new SkinnedMeshRenderer[3];

    void Start()
    {
        Init();
        SetMaterials();
    }

    void Init()
    {
        if (transform.parent != null)
        {
            manager = transform.parent.GetComponent<EnemyManager>();
            manager.enemies.Add(this);
            EditorUtility.SetDirty(manager);
        }

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < transform.GetChild(i + 1).childCount; j++)
            {
                transform.GetChild(i + 1).GetChild(j).gameObject.SetActive(false);
            }
        }

        SetMaterials();
        UpdateStyle(enemyStyle);

        //clip = getClip(transform.GetChild(1).GetComponent<Animator>().runtimeAnimatorController, "Pistol_SprintLoop");
        UpdateRect();
        EditorUtility.SetDirty(this);
    }

    void OnEnable()
    {
        SetMaterials();
        manager = transform.parent.GetComponent<EnemyManager>();
        actions.Clear();
        actions.Add(new ActionMove(new WorldPoint(new Vector3(0, 0, 15), Quaternion.identity)));
        actions[0].duration = 5;
        UpdateDuration();
        manager.enemies.Add(this);
    }

    public void UpdateStyle(int style)
    {
        enemyStyle = style;

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < transform.GetChild(i + 1).childCount; j++)
            {
                transform.GetChild(i + 1).GetChild(j).gameObject.SetActive(false);
            }
        }

        transform.GetChild(style + 1).GetChild((int)type).gameObject.SetActive(true);
    }

    public void UpdateEnemy(float songTime, Color currentEnemyColor)
    {
        UpdateOverlayColor(currentEnemyColor);

        if (songTime >= (time - duration * playerActionLerp) && songTime <= despawn)
        {
            UpdateVisibility(1);
            enemyActive = true;
        }
        else
        {
            UpdateVisibility(0);
            enemyActive = false;
        }
    }

    public void UpdateRect()
    {
        bool despawnAction = false;
        despawn = 0;

        for (int i = actions.Count - 1; i >= 0; i--)
        {
            if (actions[i].actionType == Action.ActionType.Despawn)
            {
                despawn = actions[i].sequenceStartTime;
                despawnAction = true;
            }
        }

        if (!despawnAction)
        {
            despawn = ((time + duration) * manager.manager.levelInfo.moveSpeed + 10) / manager.manager.levelInfo.moveSpeed;

            for (int i = 0; i < actions.Count; i++)
            {
                if (actions[i].actionType == Action.ActionType.Move && despawn > actions[i].sequenceStartTime && despawn < actions[i].sequenceStartTime + actions[i].duration)
                {
                    float baseTime = actions[i].sequenceStartTime;
                    float splitTime = actions[i].duration;
                    int iterations = 10;

                    for (int j = 0; j < iterations; j++)
                    {
                        splitTime /= 2;

                        if (despawn > baseTime + splitTime)
                        {
                            baseTime = baseTime + splitTime;
                        }
                        else if (despawn == baseTime + splitTime)
                        {
                            despawn = baseTime + splitTime;
                            j = iterations;
                        }
                    }

                    despawn = baseTime;
                    i = actions.Count;
                }
            }
        }

        //Debug.Log(despawn);

        enemyRect.position = new Vector2((time - (duration * playerActionLerp)) * 100, EnemyManager.trackHeight * onTrack);
        enemyRect.size = new Vector2((despawn - (time - (duration * playerActionLerp))) * 100, EnemyManager.trackHeight);
        EditorUtility.SetDirty(this);
    }

    public void SetMaterials()
    {
        Shader enemyShader = Shader.Find("Unlit/TransparentUnlit");

        enemyMats.Clear();

        enemyMats.Add(new Material(enemyShader));
        enemyMats[0].name = "Enemy Instance Material";
        enemyMats[0].color = new Color(0, 0, 0, 1);
        enemyMats.Add(new Material(enemyShader));
        enemyMats[1].name = "Gun Instance Material";
        enemyMats[1].color = new Color(1, 1, 1, 1);
        enemyMats.Add(new Material(enemyShader));
        enemyMats[2].name = "Detail Instance Material";
        enemyMats[2].color = new Color(1, 1, 1, 1);


        foreach (MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>())
        {
            for (int i = 0; i < renderer.materials.Length; i++)
            {
                if (renderer.materials[i].name.Contains("EnemyMain") || renderer.materials[i].name.Contains("Enemy Instance Material"))
                {
                    renderer.materials[i] = enemyMats[0];
                    renderers[0] = renderer;
                }
                if (renderer.materials[i].name.Contains("EnemyGun") || renderer.materials[i].name.Contains("Gun Instance Material"))
                {
                    renderer.materials[i] = enemyMats[1];
                    renderers[1] = renderer;
                }
                if (renderer.materials[i].name.Contains("EnemyDetail") || renderer.materials[i].name.Contains("Detail Instance Material"))
                {
                    renderer.materials[i] = enemyMats[2];
                    renderers[2] = renderer;
                }
            }
        }

        foreach (SkinnedMeshRenderer renderer in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            for (int i = 0; i < renderer.materials.Length; i++)
            {
                if (renderer.materials[i].name.Contains("EnemyMain") || renderer.materials[i].name.Contains("Enemy Instance Material"))
                {
                    renderer.materials[i] = enemyMats[0];
                    srenderers[0] = renderer;
                }
                if (renderer.materials[i].name.Contains("EnemyGun") || renderer.materials[i].name.Contains("Gun Instance Material"))
                {
                    renderer.materials[i] = enemyMats[1];
                    srenderers[1] = renderer;
                }
                if (renderer.materials[i].name.Contains("EnemyDetail") || renderer.materials[i].name.Contains("Detail Instance Material"))
                {
                    renderer.materials[i] = enemyMats[2];
                    srenderers[2] = renderer;
                }
            }
        }

        EditorManager.ClearConsole();
    }

    void UpdateVisibility(int vis)
    {
        foreach (Renderer rend in renderers)
        {
            if (rend != null)
            {
                foreach (Material mat in rend.materials)
                {
                    Color col = mat.color;
                    col.a = vis;
                    mat.color = col;
                }
            }
        }

        foreach (SkinnedMeshRenderer rend in srenderers)
        {
            if (rend != null)
            {
                foreach (Material mat in rend.materials)
                {
                    Color col = mat.color;
                    col.a = vis;
                    mat.color = col;
                }
            }
        }
    }

    void UpdateOverlayColor(Color col)
    {
        foreach (Renderer rend in renderers)
        {
            if (rend != null)
            {
                foreach (Material mat in rend.materials)
                {
                    mat.SetColor("_OverlayColor", col);
                }
            }
        }

        foreach (SkinnedMeshRenderer rend in srenderers)
        {
            if (rend != null)
            {
                foreach (Material mat in rend.materials)
                {
                    mat.SetColor("_OverlayColor", col);
                }
            }
        }
    }

    public void AnimateEnemy(float songTime)
    {
#if UNITY_EDITOR
        //AnimationMode.StartAnimationMode();
        //.SampleAnimationClip(transform.GetChild(enemyStyle).gameObject, clip, songTime % clip.length);
#endif
        if (enemyActive)
        {
            transform.rotation = Quaternion.Euler(0, songTime * 180, 0);
        }
    }

    public void UpdateDuration()
    {
        duration = 0;
        for (int i = 0; i < actions.Count; i++)
        {
            duration += actions[i].duration;
            if (i == 0)
            {
                actions[i].sequenceStartTime = time;
            }
            else
            {
                actions[i].sequenceStartTime = actions[i - 1].sequenceStartTime + actions[i - 1].duration;
            }
            if (actions[i].actionType == Action.ActionType.Despawn)
            {
                i = actions.Count;
            }
        }

        //Debug.Log(duration);

        UpdateRect();
    }

    public AnimationClip getClip(RuntimeAnimatorController controller, string clipName)
    {
        foreach (AnimationClip clip in controller.animationClips)
        {
            if (clip.name == clipName)
            {
                return clip;
            }
        }
        return null;
    }

    public enum Distance
    {
        Near = 4,
        Middle = 12,
        Far = 24
    }

    public enum EnemyType
    {
        Slot0 = 0,
        Slot1 = 1,
        Slot2 = 2,
        Slot4 = 3,
        Slot5 = 4,
        Slot7 = 5,
        Slot8 = 6
    }

    public enum EnemySet1
    {
        Enemy_Normal = 0,
        Enemy_Tough = 1,
        Enemy_ChuckNorris = 2,
        Enemy_2089_Minigun = 3,
        Enemy_Shield = 4,
        Enemy_2089_SentyGun = 5,
        Enemy_HorseRider = 6
    }

    public enum EnemySet2
    {
        Enemy_2089_1Shot = 0,
        Enemy_2089_2Shot = 1,
        Enemy_2089_4Shot = 2,
        Enemy_2089_Minigun = 3,
        Enemy_2089_Shield = 4,
        Enemy_2089_SentyGun = 5,
        Enemy_AP2_HorseRider = 6
    }

    public enum EnemySet3
    {
        Enemy_AP2_1Shot = 0,
        Enemy_AP2_2Shot = 1,
        Enemy_AP2_4Shot = 2,
        Enemy_2089_Minigun = 3,
        Enemy_Shield = 4,
        Enemy_2089_SentyGun = 5,
        Enemy_AP2_HorseRider = 6
    }

    public enum CheevoID
    {
        None = 0,
        Dancer = 1,
        Replicant = 2,
        Bridge = 3,
        MAX = 4,
        Any = 2147483647
    }

    public enum Facing
    {
        Forward = 0,
        //Player = 1,
        Custom = 2
    }

    public override string ToString()
    {
        var jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new ColorConverter(),
                new VectorConverter(),
                new QuaternionConverter(),
                new StringEnumConverter()
            }
        };

        return JsonConvert.SerializeObject(this, jsonSettings);
    }
}
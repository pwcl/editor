﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using UnityEditor.SceneManagement;
using Newtonsoft.Json.Converters;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class LightingManager : MonoBehaviour
{
    [SerializeField]
    public List<Section> trackSections = new List<Section>(0);
    [SerializeField]
    public List<ColorShift> colorShifts = new List<ColorShift>(0);
    public Color currentEnemyColor;

    void Start()
    {
        RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
        RenderSettings.ambientSkyColor = Color.white;

        var lights = FindObjectsOfType(typeof(Light)) as Light[];
        foreach (Light light in lights)
        {
            DestroyImmediate(light.gameObject);
        }

        GameObject newLight = new GameObject("Level Light");
        newLight.transform.parent = transform;
        Light nLight = newLight.AddComponent<Light>();
        nLight.type = LightType.Directional;
        nLight.color = Color.white;
        nLight.lightmapBakeType = LightmapBakeType.Mixed;
        nLight.intensity = 1;
        nLight.bounceIntensity = 1;
        nLight.shadows = LightShadows.Soft;
        nLight.transform.rotation = Quaternion.Euler(75, 0, 0);

        if (trackSections.Count == 0)
        {
            LevelInfo info = EditorManager.Instance.levelInfo;
            trackSections.Add(new Section(0, Mathf.CeilToInt(info.songLength), info.bpm, false, info.fogColor, info.glowColor, info.mainColor, info.enemyColor));
        }
    }


    public void UpdateColors(float time)
    {
        LevelInfo info = EditorManager.Instance.levelInfo;
        if (trackSections.Count == 0)
        {
            trackSections.Add(new Section(0, info.songLength, info.bpm, false, info.fogColor, info.glowColor, info.mainColor, info.enemyColor));
            EditorUtility.SetDirty(this);
        }
        trackSections[0].fogColor = info.fogColor;
        trackSections[0].glowColor = info.glowColor;
        trackSections[0].mainColor = info.mainColor;
        trackSections[0].enemyColor = info.enemyColor;

        RenderSettings.ambientSkyColor = trackSections[0].mainColor;
        GetComponentInChildren<Light>().color = trackSections[0].mainColor;
        RenderSettings.skybox.SetColor("_FogColor", trackSections[0].fogColor);
        currentEnemyColor = trackSections[0].enemyColor;

        int pastShift = -1;

        for (int i = 0; i < colorShifts.Count; i++)
        {
            if (time > colorShifts[i].start)
            {
                pastShift = i;
            }
        }

        if (pastShift != -1)
        {
            ColorShift lastShift = colorShifts[pastShift];

            if (lastShift.end < time)
            {
                RenderSettings.ambientSkyColor = lastShift.colors.mainColor;
                GetComponentInChildren<Light>().color = lastShift.colors.mainColor;
                RenderSettings.skybox.SetColor("_FogColor", lastShift.colors.fogColor);
                currentEnemyColor = lastShift.colors.storedEnemyColor;
            } else
            {
                ColorData lastColors = pastShift == 0 ? new ColorData(trackSections[0].mainColor, trackSections[0].fogColor, trackSections[0].glowColor, trackSections[0].enemyColor) : colorShifts[pastShift - 1].colors;
                float timeInShift = (time - lastShift.start) / (lastShift.end - lastShift.start);
                Color lastMain = Color.Lerp(lastColors.mainColor, lastShift.colors.mainColor, timeInShift);
                Color lastFog = Color.Lerp(lastColors.fogColor, lastShift.colors.fogColor, timeInShift);
                Color lastEnemy = Color.Lerp(lastColors.storedEnemyColor, lastShift.colors.storedEnemyColor, timeInShift);

                RenderSettings.ambientSkyColor = lastMain;
                GetComponentInChildren<Light>().color = lastMain;
                RenderSettings.skybox.SetColor("_FogColor", lastFog);
                currentEnemyColor = lastEnemy;
            }
        }
    }

    public void InsertShift(ColorShift shift)
    {
        int placeAt = 0;

        for (int i = 0; i < colorShifts.Count; i++)
        {
            if (shift.start > colorShifts[i].end)
            {
                placeAt = i + 1;
            }
        }

        colorShifts.Insert(placeAt, shift);
    }

    public void Save()
    {
        string sectionSavePath = Path.Combine(new string[3] { "Saved Data", EditorSceneManager.GetActiveScene().name, "Sections.json" });
        string colorSavePath = Path.Combine(new string[3] { "Saved Data", EditorSceneManager.GetActiveScene().name, "Colors.json" });

        var jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new ColorConverter(),
                new VectorConverter(),
                new QuaternionConverter(),
                new StringEnumConverter()
            }
        };

        File.WriteAllText(sectionSavePath, JsonConvert.SerializeObject(trackSections, jsonSettings));
        File.WriteAllText(colorSavePath, JsonConvert.SerializeObject(colorShifts, jsonSettings));
    }

    public void Load()
    {
        string sectionSavePath = Path.Combine(new string[3] { "Saved Data", EditorSceneManager.GetActiveScene().name, "Sections.json" });
        string colorSavePath = Path.Combine(new string[3] { "Saved Data", EditorSceneManager.GetActiveScene().name, "Colors.json" });

        if (File.Exists(sectionSavePath)) trackSections = JsonConvert.DeserializeObject<TrackData>(File.ReadAllText(sectionSavePath)).trackSections;
        if (File.Exists(colorSavePath)) colorShifts = JsonConvert.DeserializeObject<Colors>(File.ReadAllText(colorSavePath)).colorShifts;
    }
}

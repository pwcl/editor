﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class SoundManager : MonoBehaviour
{
    public AudioSource source;
    public List<Texture2D> waveformTextures = new List<Texture2D>(0);
    [SerializeField]
    public string clipPath = "";
    [SerializeField]
    public float waveformTextureWidth = 0;

    public bool playing
    {
        get
        {
            return source.isPlaying;
        }
    }

    public AudioClip clip
    {
        get
        {
            return source.clip;
        }

        set
        {
            source.clip = value;
        }
    }

    public float songTime
    {
        get
        {
            if (source == null)
            {
                return -1;
            }
            return source.time;
        }

        set
        {
            source.time = value;
        }
    }

    void Start()
    {
        source = GetComponent<AudioSource>();
        EditorUtility.SetDirty(this);
    }

    void OnValidate()
    {
        if (AssetDatabase.GetAssetPath(clip) != clipPath)
        {
            clipPath = AssetDatabase.GetAssetPath(clip);

            UpdateWaveform();
        }

        if (source == null)
        {
            source = GetComponent<AudioSource>();
        }

        waveformTextureWidth = 0;
        if (waveformTextures.Count != 0 && waveformTextures[0] == null)
        {
            waveformTextures.Clear();
            UpdateWaveform();
        }
        foreach (Texture2D tex in waveformTextures)
        {
            if (tex != null)
            {
                waveformTextureWidth += tex.width;
            }
        }
    }

    void Update()
    {

    }

    public void UpdateSong(float time, int playState)
    {
        if (time != -1)
        {
            songTime = time;
        }

        switch (playState)
        {
            case 0:
                source.Play();
                break;

            case 1:
                source.Pause();
                break;

            default:
                source.Stop();
                break;
        }
    }

    public AudioClip GetSong()
    {
        return clip;
    }

    public void SetSong(AudioClip newSong)
    {
        clip = newSong;

        EditorUtility.SetDirty(this);
    }

    public void UpdateWaveform()
    {
        waveformTextureWidth = 0;
        waveformTextures = new List<Texture2D>(0);
        AudioImporter importer = AudioImporter.GetAtPath(AssetDatabase.GetAssetPath(clip)) as AudioImporter;
        importer.forceToMono = true;
        importer.SaveAndReimport();

        int clipSampleRate = Mathf.RoundToInt(clip.samples / clip.length);

        float[] samples = new float[clip.samples];
        clip.GetData(samples, 0);

        int textureSeconds = 30;

        for (int sec = 0; sec < Mathf.CeilToInt(clip.length / textureSeconds); sec++)
        {
            int secTime = (sec + 1) * textureSeconds <= clip.length ? textureSeconds : Mathf.CeilToInt(clip.length % (sec * textureSeconds));

            int secOffset = (sec * textureSeconds) * clipSampleRate;
            int endLoop = (sec + 1) * textureSeconds <= clip.length ? (secTime * clipSampleRate) : clip.samples - secOffset;

            Texture2D tex = new Texture2D(100, (endLoop / (clipSampleRate / 100)));
            waveformTextureWidth += endLoop / (clipSampleRate / 100);
            List<Color> colorList = new List<Color>(0);
            //Debug.Log("section " + sec);
            //Debug.Log("start sample: " + secOffset);
            //Debug.Log("end sample: " + (secOffset+endLoop));

            for (int i = 0; i < endLoop; i += (clipSampleRate / 100))
            {
                int thick = Mathf.FloorToInt(Mathf.Abs(samples[secOffset + i]) * 100);
                if (thick % 2 != 0)
                {
                    thick++;
                }
                int padding = (100 - thick) / 2;

                for (int j = 0; j < padding; j++)
                {
                    colorList.Add(new Color(0.33f, 0.33f, 0.33f));
                }
                for (int j = 0; j < thick; j++)
                {
                    colorList.Add(new Color(1f, 1f, 1f));
                }
                for (int j = 0; j < padding; j++)
                {
                    colorList.Add(new Color(0.33f, 0.33f, 0.33f));
                }
            }

            tex.SetPixels(colorList.ToArray());
            tex.Apply(false);

            waveformTextures.Add(Rotate_Texture2d(tex));
        }

        importer.forceToMono = false;
        importer.SaveAndReimport();

        EditorUtility.SetDirty(this);
    }


    Texture2D Rotate_Texture2d(Texture2D tex)
    {
        Texture2D rotated = new Texture2D(tex.height, tex.width);
        List<Color> flatArray = new List<Color>(0);

        for (int i = 0; i < tex.width; i++)
        {
            flatArray.AddRange(tex.GetPixels(i, 0, 1, tex.height));
        }
        rotated.SetPixels(flatArray.ToArray());
        rotated.Apply(false);
        return rotated;
    }
}

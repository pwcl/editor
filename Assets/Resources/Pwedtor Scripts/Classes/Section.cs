﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;
using UnityEditor;
using Newtonsoft.Json.Converters;

[JsonObject(MemberSerialization.OptIn)]
[Serializable]
public class Section
{
    [JsonProperty]
    public string name = "";
    [JsonProperty]
    public float start;
    [JsonProperty]
    public float end;
    [JsonProperty]
    public float bpm;
    [JsonProperty]
    public bool beat;
    [JsonProperty]
    public float colorHue;
    [JsonProperty]
    public float colorSaturation;
    [JsonProperty]
    public float colorValue;
    [JsonProperty]
    public Color fogColor;
    [JsonProperty]
    public Color glowColor;
    [JsonProperty]
    public Color mainColor;
    [JsonProperty]
    public Color enemyColor;
    [JsonProperty]
    public bool customEnemyColor = false;

    public Section(float start, float end, float bpm, bool beat, Color fogColor, Color glowColor, Color mainColor, Color enemyColor)
    {
        this.start = start;
        this.end = end;
        this.bpm = bpm;
        this.beat = beat;
        this.fogColor = fogColor;
        this.glowColor = glowColor;
        this.mainColor = mainColor;
        this.enemyColor = enemyColor;
    }
}

public class TrackData
{
    public List<Section> trackSections = new List<Section>(0);

    public TrackData(List<Section> sections)
    {
        trackSections = sections;
    }

    public override string ToString()
    {
        var jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new ColorConverter(),
                new VectorConverter(),
                new QuaternionConverter(),
                new StringEnumConverter()
            }
        };

        return JsonConvert.SerializeObject(this, jsonSettings);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;

[JsonObject(MemberSerialization.OptIn)]
public class DecoratorCube : MonoBehaviour
{
    [JsonProperty]
    public Vector3 restPoint;
    [JsonProperty]
    public float moveScale;
    [JsonProperty]
    public float phase;

    private void Update()
    {
        restPoint = transform.position;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionFire : Action
{
    public ActionFire()
    {
        action = ActionType.Fire;
    }
}

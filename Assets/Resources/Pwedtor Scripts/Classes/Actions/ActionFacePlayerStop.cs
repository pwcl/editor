﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionFacePlayerStop : Action
{
    public ActionFacePlayerStop()
    {
        action = ActionType.FacePlayerStop;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionStopFiring : Action
{
    public ActionStopFiring()
    {
        action = ActionType.StopFiring;
    }
}

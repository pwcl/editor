﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAimWithoutFacing : Action
{
    public ActionAimWithoutFacing()
    {
        action = ActionType.AimWithoutFacing;
    }
}

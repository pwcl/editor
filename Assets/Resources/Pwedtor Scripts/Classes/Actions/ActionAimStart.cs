﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAimStart : Action
{
    public ActionAimStart()
    {
        action = ActionType.AimStart;
    }
}

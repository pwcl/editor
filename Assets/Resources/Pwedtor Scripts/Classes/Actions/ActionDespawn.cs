﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionDespawn : Action
{
    public ActionDespawn()
    {
        action = ActionType.Despawn;
    }
}
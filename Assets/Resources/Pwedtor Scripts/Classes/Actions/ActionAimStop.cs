﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAimStop : Action
{
    public ActionAimStop()
    {
        action = ActionType.AimStop;
    }
}

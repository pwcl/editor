﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using System;

[JsonObject(MemberSerialization.OptIn)]
[Serializable]
public class Action
{
    // All
    [JsonProperty]
    public ActionType action;
    public ActionType actionType
    {
        get
        {
            return action;
        }

        set
        {
            action = value;
            if (value != ActionType.Move && value != ActionType.Wait)
            {
                duration = 0;
            }
        }
    }
    [JsonProperty]
    public float duration;
    [JsonProperty]
    public float sequenceStartTime;
    public bool inspecting = false;

    //Move
    [JsonProperty]
    public bool isOverrideAnimationSpeed = false;
    [JsonProperty]
    public int animationSpeedOverride = 0;
    [JsonProperty]
    public WorldPoint destination = new WorldPoint(Vector3.zero, Quaternion.identity);
    public bool inspectingDest = false;

    // Aim and Fire
    [JsonProperty]
    public float fireTime;
    [JsonProperty]
    public bool stopFacingOnExit;
    [JsonProperty]
    public bool stopLookingOnExit;

    public Action Duplicate()
    {
        Action copy = (Action)this.MemberwiseClone();
        copy.destination = destination.Duplicate();
        return copy;
    }

    public enum ActionType
    {
        Wait = 0,
        Move = 1,
        AimStart = 2,
        AimAndFire = 3,
        Fire = 4,
        AimStop = 5,
        //Animation = 6,
        StopFiring = 7,
        Despawn = 8,
        FacePlayerStart = 9,
        FacePlayerStop = 10,
        AimWithoutFacing = 11
        //Strafe = 12
        //Instant = 13
    }

    // Might update, but there are a lot of actions
    public static Dictionary<ActionType, Color32> actionColors = new Dictionary<ActionType, Color32>(0)
    {
        [ActionType.Wait] = new Color32(64, 244, 244, 255),
        [ActionType.Move] = new Color32(154, 64, 244, 255),
        [ActionType.AimStart] = new Color32(255, 251, 83, 255),
        [ActionType.AimAndFire] = new Color32(255, 176, 40, 255),
        [ActionType.Fire] = new Color32(244, 64, 64, 255),
        [ActionType.AimStop] = new Color32(154, 244, 64, 255)
    };
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionFacePlayerStart : Action
{
    public ActionFacePlayerStart()
    {
        action = ActionType.FacePlayerStart;
    }
}

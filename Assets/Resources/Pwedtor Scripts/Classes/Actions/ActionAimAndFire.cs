﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAimAndFire : Action
{
    public ActionAimAndFire(float FireTime, bool stopFacing, bool stopLooking)
    {
        action = ActionType.AimAndFire;
        fireTime = FireTime;
        stopFacingOnExit = stopFacing;
        stopLookingOnExit = stopLooking;
    }
}

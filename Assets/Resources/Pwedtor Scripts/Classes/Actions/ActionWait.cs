﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionWait : Action
{
    public ActionWait()
    {
        action = ActionType.Wait;
    }

    public ActionWait(float time)
    {
        action = ActionType.Wait;
        duration = time;
    }
}

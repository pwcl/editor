﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class ActionMove : Action
{

    public ActionMove()
    {
        action = ActionType.Move;
    }

    public ActionMove(WorldPoint Destination)
    {
        action = ActionType.Move;
        destination = Destination;
    }

    public ActionMove(WorldPoint Destination, int speed)
    {
        action = ActionType.Move;
        destination = Destination;
        isOverrideAnimationSpeed = true;
        animationSpeedOverride = speed;
    }
}

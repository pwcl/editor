﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Threading.Tasks;
using Newtonsoft.Json;


[JsonObject(MemberSerialization.OptIn)]
[Serializable]
public class WorldPoint
{
    [JsonProperty]
    public Vector3 position;
    [JsonProperty]
    public Quaternion rotation = Quaternion.identity;
    public Vector3 rotationBuffer = Vector3.zero;
    public Vector3 RotationBuffer
    {
        get
        {
            return rotationBuffer;
        }

        set
        {
            rotationBuffer = value;
            rotation = Quaternion.Euler(value);
        }
    }

    public WorldPoint()
    {
        position = Vector3.zero;
        rotation = Quaternion.identity;
    }

    public WorldPoint(Vector3 pos, Quaternion rot)
    {
        position = pos;
        rotation = rot;
    }

    public WorldPoint Duplicate()
    {
        return (WorldPoint)this.MemberwiseClone();
    }
}

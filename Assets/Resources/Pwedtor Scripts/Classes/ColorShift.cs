﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

[JsonObject(MemberSerialization.OptIn)]
[Serializable]
public class ColorData
{
    [JsonProperty]
    public Color fogColor = new Color(0.5f, 0.5f, 0.5f, 1f);
    [JsonProperty]
    public Color glowColor = new Color(0.5f, 0.5f, 0.5f, 1f);
    [JsonProperty]
    public Color mainColor = new Color(0.5f, 0.5f, 0.5f, 1f);
    [JsonProperty]
    public bool customEnemyColor = true;
    [JsonProperty]
    public Color storedEnemyColor = new Color(0.5f, 0.5f, 0.5f, 1f);


    public ColorData(Color main, Color fog, Color glow, Color enemy)
    {
        mainColor = main;
        fogColor = fog;
        glowColor = glow;
        storedEnemyColor = enemy;
    }
}

[Serializable]
public class ColorShift
{
    public float start = 0;
    public float end = 0;
    public ColorData colors;

    public ColorShift(float start, float end, ColorData colors)
    {
        this.start = start;
        this.end = end;
        this.colors = colors;
    }
}

public class Colors
{
    public List<ColorShift> colorShifts = new List<ColorShift>(0);

    public Colors(List<ColorShift> colors)
    {
        colorShifts = colors;
    }

    public override string ToString()
    {
        var jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new ColorConverter(),
                new VectorConverter(),
                new QuaternionConverter(),
                new StringEnumConverter()
            }
        };

        return JsonConvert.SerializeObject(this, jsonSettings);
    }
}
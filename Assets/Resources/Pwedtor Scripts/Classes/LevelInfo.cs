﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Reflection;

[Serializable]
public class LevelInfo
{
    public float version = 0.5f;
    public string songClip;
    public string songArtists;
    public string songName;
    public string mapper;
    public float bpm;
    public float songLength;
    public string description;
    public float previewTime;
    public EnemySets enemyStyle;
    public ObstacleSets obstacleStyle;
    public MaterialPropertiesSet materialStyle;
    public float moveSpeed = 3;
    public float timingWindowSize = 150;
    public Color mainColor = new Color(0.5f, 0.5f, 0.5f, 1);
    public Color fogColor = new Color(0.5f, 0.5f, 0.5f, 1);
    public Color glowColor = new Color(0.5f, 0.5f, 0.5f, 1);
    public Color enemyColor = new Color(0.5f, 0.5f, 0.5f, 1);
    public bool[] hasMaps = new bool[3] { false, false, false };

    public enum EnemySets
    {
        Normal = 0,
        Robots = 1,
        Outlaws = 2
    }

    public enum ObstacleSets
    {
        Normal = 0,
        Spooky = 1,
        Pipes = 2,
        Rocks = 3,
        AirDrop = 4,
        Colony = 5,
        Tower = 6,
        EarthCracker = 7,
        Crates = 8,
        Train = 9
    }

    public enum MaterialPropertiesSet
    {
        Default = 0,
        Heartbreaker = 1,
        AlienPlanet_2089 = 2,
        Arbiter_2089 = 3,
        RobotFacilities_2089 = 4,
        StrangeCreatures_2089 = 5,
        Cave_AP2 = 6,
        Desert_AP2 = 7,
        WesternTown = 8,
        OldWestTrain = 9
    }

    public enum MoveMode
    {
        Moving = 0,
        Stationary = 1
    }

    public override string ToString()
    {
        var jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new ColorConverter(),
                new VectorConverter(),
                new QuaternionConverter(),
                new StringEnumConverter()
            }
        };

        return JsonConvert.SerializeObject(this, jsonSettings);
    }

    public LevelInfo Clone()
    {
        return this.MemberwiseClone() as LevelInfo;
    }

    public bool CheckSame(LevelInfo check)
    {
        if (songClip != check.songClip ||
            songArtists != check.songArtists ||
            songName != check.songName ||
            mapper != check.mapper ||
            bpm != check.bpm ||
            songLength != check.songLength ||
            description != check.description ||
            previewTime != check.previewTime ||
            enemyStyle != check.enemyStyle ||
            obstacleStyle != check.obstacleStyle ||
            materialStyle != check.materialStyle ||
            moveSpeed != check.moveSpeed ||
            timingWindowSize != check.timingWindowSize ||
            mainColor != check.mainColor ||
            fogColor != check.fogColor ||
            glowColor != check.glowColor ||
            enemyColor != check.enemyColor ||
            hasMaps[0] != check.hasMaps[0] ||
            hasMaps[1] != check.hasMaps[1] ||
            hasMaps[2] != check.hasMaps[2]
            )
        {
            return false;
        } else
        {
            return true;
        }
    }
}

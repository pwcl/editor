﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Newtonsoft.Json;
using UnityEditor;

[JsonObject(MemberSerialization.OptIn)]
[Serializable]
public class Koreography
{
    [JsonProperty]
    public int mSampleRate = 44100;
    [JsonProperty]
    public List<TempoSectionDef> mTempoSections = new List<TempoSectionDef>(0);
    [JsonProperty]
    public List<KoreographyTrackBase> mTracks = new List<KoreographyTrackBase>(3)
    {
        new KoreographyTrackBase("Beat"),
        new KoreographyTrackBase("NoBeat"),
        new KoreographyTrackBase("Event")
    };
    public KoreographyTrackBase beatTrack;
    public KoreographyTrackBase noBeatTrack;
    public KoreographyTrackBase eventTrack;

    public Koreography()
    {
        beatTrack = mTracks[0];
        noBeatTrack = mTracks[1];
        eventTrack = mTracks[2];
    }

    public void Update()
    {
        foreach (TempoSectionDef section in mTempoSections)
        {
            section.samplesPerBeat = (mSampleRate * 60) / section.bpm;
        }
    }

    /*public static Koreography Generate(TrackData data)
    {
        Koreography koreo = new Koreography();

        if (EditorManager.Instance.soundManager.clip != null)
        {
            koreo.mSampleRate = Mathf.RoundToInt(EditorManager.Instance.soundManager.clip.samples / EditorManager.Instance.soundManager.clip.length);
            //Debug.Log("sample~rate: " + koreo.mSampleRate);

            foreach (TSection section in data.sections)
            {
                TempoSectionDef tempoSec = new TempoSectionDef();
                tempoSec.beatsPerMeasure = 4;
                tempoSec.startSample = section.start * koreo.mSampleRate;
                tempoSec.sectionName = section.name;
                tempoSec.bStartNewMeasure = true;
                if (section.beat)
                {
                    //Debug.Log("section~bpm: " + section.bpm);
                    tempoSec.bpm = section.bpm;
                    tempoSec.samplesPerBeat = (koreo.mSampleRate * 60) / section.bpm;
                    for (int i = section.start * koreo.mSampleRate; i < section.end * koreo.mSampleRate; i += Mathf.FloorToInt((float)tempoSec.samplesPerBeat))
                    {
                        if (i <= EditWindow.songClip.samples)
                        {
                            koreo.mTracks[0].mEventList.Add(new KoreographyEvent(i));
                        }
                    }

                }
                else
                {
                    tempoSec.bpm = 0;
                    tempoSec.samplesPerBeat = 0;
                    koreo.mTracks[1].mEventList.Add(new KoreographyEvent(section.start * koreo.mSampleRate, section.end * koreo.mSampleRate));
                }
                koreo.mTempoSections.Add(tempoSec);
            }
        }

        return koreo;
    }*/
}

public class TempoSectionDef
{
    public float bpm;
    public string sectionName;
    public int startSample;
    public double samplesPerBeat;
    public int beatsPerMeasure = 4;
    public bool bStartNewMeasure = true;
}

public class KoreographyTrackBase
{
    public string mEventID;
    public List<KoreographyEvent> mEventList = new List<KoreographyEvent>(0);

    public KoreographyTrackBase(string id)
    {
        mEventID = id;
    }
}

public class KoreographyEvent
{
    public int mStartSample;
    public int mEndSample;

    public KoreographyEvent(int sample)
    {
        mStartSample = sample;
        mEndSample = sample + 1;
    }

    public KoreographyEvent(int start, int end)
    {
        mStartSample = start;
        mEndSample = end;
    }
}
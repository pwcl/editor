﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PWInstpector : EditorWindow
{
    static PWInstpector window;
    public static PWInstpector Instance { get { return window; } }

    static float width = 150;
    GUIStyle defaultStyle = new GUIStyle();

    static LightingManager lightingManager;

    public InspectingType currentlyInspecting = InspectingType.None;
    public int inspectingShift = -1;


    [MenuItem("Pistol Whip/PW Inspector")]
    private static void Init()
    {
        window = GetWindow<PWInstpector>();
        window.Show();
        lightingManager = EditorManager.Instance.lightingManager;
    }

    void OnValidate()
    {
        window = GetWindow<PWInstpector>();
        lightingManager = EditorManager.Instance.lightingManager;
    }

    void OnGUI()
    {
        defaultStyle.alignment = TextAnchor.MiddleCenter;
        defaultStyle.fontStyle = FontStyle.Normal;
        defaultStyle.normal.textColor = Color.white;

        Event e = Event.current;

        if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Escape)
        {
            currentlyInspecting = InspectingType.None;
        }

        switch (currentlyInspecting)
        {
            case InspectingType.None:
                break;

            case InspectingType.ColorShift:
                ShiftInspector();
                break;
        }

        Repaint();
    }

    void ShiftInspector()
    {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("<", GUILayout.Width(width * 2/3)))
        {
            inspectingShift--;
        }
        inspectingShift = EditorGUILayout.DelayedIntField(inspectingShift, defaultStyle, GUILayout.Width(width *  2/3));
        if (GUILayout.Button(">", GUILayout.Width(width * 2/3)))
        {
            inspectingShift++;
        }
        inspectingShift = Mathf.Clamp(inspectingShift, -1, lightingManager.colorShifts.Count - 1);
        GUILayout.EndHorizontal();

        if (inspectingShift == -1)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Main Color", defaultStyle, GUILayout.Width(width));
            EditorManager.Instance.levelInfo.mainColor = EditorGUILayout.ColorField(EditorManager.Instance.levelInfo.mainColor, GUILayout.Width(width));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Fog Color", defaultStyle, GUILayout.Width(width));
            EditorManager.Instance.levelInfo.fogColor = EditorGUILayout.ColorField(EditorManager.Instance.levelInfo.fogColor, GUILayout.Width(width));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Glow Color", defaultStyle, GUILayout.Width(width));
            EditorManager.Instance.levelInfo.glowColor = EditorGUILayout.ColorField(EditorManager.Instance.levelInfo.glowColor, GUILayout.Width(width));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Enemy Color", defaultStyle, GUILayout.Width(width));
            EditorManager.Instance.levelInfo.enemyColor = EditorGUILayout.ColorField(EditorManager.Instance.levelInfo.enemyColor, GUILayout.Width(width));
            GUILayout.EndHorizontal();
        } else
        {
            GUILayout.BeginHorizontal();
            lightingManager.colorShifts[inspectingShift].start = EditorGUILayout.FloatField( new GUIContent("Shift Start"), lightingManager.colorShifts[inspectingShift].start, GUILayout.Width(width * 2));
            GUILayout.EndHorizontal();

            if (inspectingShift == 0)
            {
                lightingManager.colorShifts[inspectingShift].start = Mathf.Clamp(lightingManager.colorShifts[inspectingShift].start, 0, lightingManager.colorShifts[inspectingShift].end);
            }
            else
            {
                lightingManager.colorShifts[inspectingShift].start = Mathf.Clamp(lightingManager.colorShifts[inspectingShift].start, lightingManager.colorShifts[inspectingShift - 1].end, lightingManager.colorShifts[inspectingShift].end);
            }

            GUILayout.BeginHorizontal();
            lightingManager.colorShifts[inspectingShift].end = EditorGUILayout.FloatField(new GUIContent("Shift End"), lightingManager.colorShifts[inspectingShift].end, GUILayout.Width(width * 2));
            GUILayout.EndHorizontal();

            if (inspectingShift == lightingManager.colorShifts.Count - 1)
            {
                lightingManager.colorShifts[inspectingShift].end = Mathf.Clamp(lightingManager.colorShifts[inspectingShift].end, lightingManager.colorShifts[inspectingShift].start, EditorManager.Instance.levelInfo.songLength);
            }
            else
            {
                lightingManager.colorShifts[inspectingShift].end = Mathf.Clamp(lightingManager.colorShifts[inspectingShift].end, lightingManager.colorShifts[inspectingShift].start, lightingManager.colorShifts[inspectingShift + 1].start);
            }

            GUILayout.BeginHorizontal();
            lightingManager.colorShifts[inspectingShift].colors.mainColor = EditorGUILayout.ColorField( new GUIContent("Main Color"), lightingManager.colorShifts[inspectingShift].colors.mainColor, GUILayout.Width(width * 2));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            lightingManager.colorShifts[inspectingShift].colors.fogColor = EditorGUILayout.ColorField( new GUIContent("Fog Color"), lightingManager.colorShifts[inspectingShift].colors.fogColor, GUILayout.Width(width * 2));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            lightingManager.colorShifts[inspectingShift].colors.glowColor = EditorGUILayout.ColorField( new GUIContent("Glow Color"), lightingManager.colorShifts[inspectingShift].colors.glowColor, GUILayout.Width(width * 2));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            lightingManager.colorShifts[inspectingShift].colors.storedEnemyColor = EditorGUILayout.ColorField( new GUIContent("Enemy Color"), lightingManager.colorShifts[inspectingShift].colors.storedEnemyColor, GUILayout.Width(width * 2));
            GUILayout.EndHorizontal();
        }
    }

    public enum InspectingType
    {
        None = -1,
        Enemy = 0,
        ColorShift = 1
    }
}

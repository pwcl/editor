﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using System;

[ExecuteInEditMode]
public class EditWindow : EditorWindow
{
    static EditWindow window;

    public static EditorManager manager;

    public static EditorTabManager tabManager;
    public static AudioDisplayManager audioDisplayManager;
    public static LevelInfoTab tab_LevelInfo;
    public static EnemyPlacementTab tab_EnemyPlacement;
    public static LevelColorsTab tab_LevelColors;
    static Event e;
    static Vector2 scroll = Vector2.zero;
    static Vector2 zoom = Vector2.one;
    static Vector2 maxScroll = new Vector2(-Mathf.Infinity, -Mathf.Infinity);

    public static EditWindow Instance { get { return window; } }

    [MenuItem("Pistol Whip/Edit Window")]
    private static void Init()
    {
        InitWindow();
        window = GetWindow<EditWindow>();
        window.Show();
    }

    private void OnValidate()
    {
        CreateTabs();
    }

    private static void SceneOpen(string path, OpenSceneMode mode)
    {
        GetWindow<EditWindow>().Close();
    }

    static void InitWindow()
    {
        if (GameObject.Find("Editor Manager") == null)
        {
            GameObject managerObject = GameObject.Instantiate(Resources.Load<GameObject>("PW Assets/Editor Manager"));
            managerObject.name = "Editor Manager";
        }

        manager = GameObject.Find("Editor Manager").GetComponent<EditorManager>();
        EditorSceneManager.sceneOpening += SceneOpen;

        CreateTabs();
    }

    static void CreateTabs()
    {
        tabManager = new EditorTabManager(Instance, new List<string>(1) { "Level Info", "Enemy Setup", "Enemy Placement", "Beats and BPM", "Level Colors", "Decorations" }, 0);
        audioDisplayManager = new AudioDisplayManager(Instance);
        tab_LevelInfo = new LevelInfoTab(Instance);
        tab_EnemyPlacement = new EnemyPlacementTab(Instance, manager.enemyManager);
        tab_LevelColors = new LevelColorsTab(Instance);
    }

    void OnGUI()
    {
        e = Event.current;

        switch (e.type)
        {
            case EventType.ScrollWheel:
                float mouseo;
                float mousen;
                float offset;
                if (e.alt && e.control)
                {
                    mouseo = (e.mousePosition.y) * zoom.y;

                    zoom.y += e.delta.y / 30;
                    zoom.y = Mathf.Clamp(zoom.y, 0.1f, 10);

                    mousen = (e.mousePosition.y) * zoom.y;

                    offset = (mousen - mouseo);
                    scroll.y += offset;
                    //Debug.Log("zoomx: " + zoom);
                }
                else if (e.alt)
                {
                    mouseo = (e.mousePosition.x) * zoom.x;

                    zoom.x += e.delta.y / 30;
                    zoom.x = Mathf.Clamp(zoom.x, 0.1f, 10);

                    mousen = (e.mousePosition.x) * zoom.x;

                    offset = (mousen - mouseo);
                    scroll.x += offset;
                    //Debug.Log("zoomx: " + zoom);
                }
                else if (!e.control)
                {
                    scroll.x -= (e.delta.y / 3) * (Convert.ToInt32(e.shift) * 4 + 1) * (zoom.x + 1);
                    //Debug.Log("scroll.x: " + scroll.x);
                }
                else
                {
                    scroll.y -= (e.delta.y / 3) * (zoom.y / 1) * 4 * (Convert.ToInt32(e.shift) * 8 + 1);
                    //Debug.Log("scroll.y: " + scroll.y);
                }
                break;

            case EventType.MouseDown:
                switch (tabManager.GetCurrentTab())
                {
                    case "Enemy Placement":
                        break;
                }
                break;
        }

        switch (tabManager.GetCurrentTab())
        {
            case "Level Info":
                tab_LevelInfo.Draw();
                break;

            case "Enemy Placement":
                tab_EnemyPlacement.Draw(scroll, zoom);
                audioDisplayManager.Draw(scroll, zoom);
                audioDisplayManager.Click(e, ref scroll, zoom);
                break;

            case "Level Colors":
                tab_LevelColors.Draw(scroll, zoom);
                tab_LevelColors.Click(e, ref scroll, zoom);
                audioDisplayManager.Draw(scroll, zoom);
                audioDisplayManager.Click(e, ref scroll, zoom);
                break;
        }

        tabManager.Draw(e);
        Repaint();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorTabManager
{
    EditWindow window;
    List<string> editorTabs;
    string currentTab;

    public Vector2 tabSize = new Vector2(100, 30);
    public bool coverEntireWidth = false;
    public Rect boundsRect;

    public void Draw(Event e)
    {
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
        GUI.skin.label.wordWrap = true;

        if (coverEntireWidth)
        {
            EditorGUI.DrawRect(new Rect(0, window.position.height - tabSize.y, window.position.width, tabSize.y), new Color(0.5f, 0.5f, 0.5f));
        }

        for (int i = 0; i < editorTabs.Count; i++)
        {
            Rect tabRect = new Rect(i * tabSize.x, window.position.height - tabSize.y, tabSize.x - 1, tabSize.y);
            if (editorTabs[i] == currentTab)
            {
                EditorGUI.DrawRect(tabRect, new Color(0f, 0f, 0f));
            }
            else if(tabRect.Contains(e.mousePosition))
            {
                EditorGUI.DrawRect(tabRect, new Color(0.2f, 0.2f, 0.2f));
            }
            else
            {
                EditorGUI.DrawRect(tabRect, new Color(0.4f, 0.4f, 0.4f));
            }
            EditorGUI.LabelField(tabRect, editorTabs[i], GUI.skin.label);
        }

        if (GetBoundsRect().Contains(e.mousePosition)&& e.type == EventType.MouseDown && e.button == 0)
        {
            ClickTab(e.mousePosition);
        } 
    }

    public void ChangeTab(string newTabName)
    {
        if (editorTabs.Contains(newTabName))
        {
            currentTab = newTabName;
        }
    }

    public void ClickTab(Vector2 mousePosition)
    {
        ChangeTab(editorTabs[Mathf.FloorToInt(mousePosition.x / tabSize.x)]);
    }

    public string GetCurrentTab()
    {
        return currentTab;
    }

    public Rect GetBoundsRect()
    {
        boundsRect = new Rect(0, window.position.height - tabSize.y, tabSize.x * editorTabs.Count, tabSize.y);
        return boundsRect;
    }

    public EditorTabManager(EditWindow parentWindow, List<string> tabs, int defaultTab)
    {
        window = EditorWindow.GetWindow<EditWindow>();
        editorTabs = tabs;
        currentTab = editorTabs[defaultTab];
    }
}
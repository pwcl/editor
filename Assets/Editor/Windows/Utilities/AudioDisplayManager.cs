﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class AudioDisplayManager
{
    EditWindow window;
    float volume = 50f;
    public static float audioDisplayHeight = 160;
    static int dragging = -1;

    public void Draw(Vector2 scroll, Vector2 zoom)
    {
        GUI.skin.label.alignment = TextAnchor.MiddleLeft;
        GUI.skin.label.wordWrap = false;

        List<Texture2D> waveforms = EditWindow.manager.soundManager.waveformTextures;

        float texOffset = 0;

        EditorGUI.DrawRect(new Rect(0, 0, 10000, audioDisplayHeight), new Color(0.25f, 0.25f, 0.25f));
        for (int i = 0; i < waveforms.Count; i++)
        {
            EditorGUI.DrawPreviewTexture(RectHelpers.WorldToScreen(new Rect(texOffset, 30, waveforms[i].width, 100), new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), waveforms[i]);
            texOffset += waveforms[i].width;
        }

        try
        {
            GUILayout.BeginHorizontal(GUILayout.Height(25), GUILayout.Width(225));
            if (GUILayout.Button("►", GUILayout.Width(25), GUILayout.Height(25)))
            {
                EditWindow.manager.UpdateSong(-1, 0);
            }
            if (GUILayout.Button("||", GUILayout.Width(25), GUILayout.Height(25)))
            {
                EditWindow.manager.UpdateSong(-1, 1);
            }
            if (GUILayout.Button("■", GUILayout.Width(25), GUILayout.Height(25)))
            {
                EditWindow.manager.UpdateSong(0, 1);
            }
            GUILayout.Label("  Volume:");
            volume = GUILayout.HorizontalSlider(volume, 0f, 100f, GUILayout.Width(100));
            EditWindow.manager.soundManager.source.volume = volume / 100;
            GUILayout.Label("  Follow Song:");
            EditWindow.manager.followAlong = GUILayout.Toggle(EditWindow.manager.followAlong, "");
            GUILayout.Label("  Song Time: " + Mathf.Round(EditWindow.manager.soundManager.songTime * 100) / 100);
            GUILayout.EndHorizontal();
        }
        catch (ArgumentException e)
        {
            ArgumentException ignore = e;
        }

        /*float viewportTime = (-scrollX / (100 - zoom));
        float viewportEndTime = viewportTime + (windowRect.width / (100 - zoom));

        if (beatTimes != null && beatTimes.Count > 0)
        {
            for (int i = 0; i < beatTimes.Count; i++)
            {
                if (beatTimes[i] > viewportEndTime)
                {
                    i = beatTimes.Count;
                }
                else if (beatTimes[i] > viewportTime)
                {
                    EditorGUI.DrawRect(new Rect(beatTimes[i] * (100 - zoom) + scroll.x - 0.5f, 30, 1, 100), new Color(1, 0.23f, 1, 1));
                }
            }
        }*/

        //clipSampleRate = Mathf.RoundToInt(SongClip.samples / SongClip.length);

        //float begin = Math.Abs(scrollX / (100 - zoom) * clipSampleRate);
        //int coverWidth = Mathf.RoundToInt((mainSource.timeSamples - begin) / (clipSampleRate / (100 - zoom)));
        if (window == null)
        {
            window = EditorWindow.GetWindow<EditWindow>();
        }

        EditorGUI.DrawRect(RectHelpers.WorldToScreen(new Rect(0, 30, EditWindow.manager.soundManager.songTime * 100, 100), new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), new Color(0.66f, 0.66f, 0.66f, 0.5f));
        EditorGUI.DrawRect(RectHelpers.WorldToScreen(new Rect(EditWindow.manager.soundManager.songTime * 100 - zoom.x, 30, zoom.x * 2, window.position.height), new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), new Color32(0, 152, 255, 255));

        float leftEdge = (-scroll.x / EditWindow.manager.soundManager.waveformTextureWidth) * window.position.width;
        float rightEdge = ((-scroll.x + (window.position.width * zoom.x)) / EditWindow.manager.soundManager.waveformTextureWidth) * window.position.width;

        EditorGUI.DrawRect(new Rect(leftEdge - 1, 130, 2, audioDisplayHeight - 130), new Color32(247, 104, 2, 255));
        EditorGUI.DrawRect(new Rect(rightEdge - 1, 130, 2, audioDisplayHeight - 130), new Color32(247, 104, 2, 255));

        /*float viewportSampleTime = (-scrollX / (100 - zoom)) / (SongClip.length / windowRect.width);
        float viewportSampleEndTime = viewportSampleTime + (windowRect.width / (100 - zoom)) / (SongClip.length / windowRect.width);

        EditorGUI.DrawRect(new Rect(viewportSampleTime - 1, 130, 2, audioDisplayHeight - 130), new Color32(247, 104, 2, 255));
        EditorGUI.DrawRect(new Rect(viewportSampleEndTime - 1, 130, 2, audioDisplayHeight - 130), new Color32(247, 104, 2, 255));

        if (followAlong && mainSource.isPlaying)
        {
            float leftEdge = Mathf.RoundToInt(Math.Abs(scrollX / (100 - zoom)));
            float visibleWidth = windowRect.width / (100 - zoom);
            float rightEdge = leftEdge + visibleWidth;
            if (mainSource.time < leftEdge)
            {
                scroll.x = Mathf.Clamp(scrollX + windowRect.width, -SongClip.length * (100 - zoom), 0);
            }
            else if (mainSource.time > rightEdge)
            {
                scroll.x = Mathf.Clamp(scrollX - windowRect.width, -SongClip.length * (100 - zoom), 0);
            }
        }*/
    }


    public void Click(Event e, ref Vector2 scroll, Vector2 zoom)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.mousePosition.y <= 30)
                {
                    dragging = 0;
                }
                else if (e.mousePosition.y <= 130)
                {
                    dragging = 1;
                }
                else if (e.mousePosition.y <= audioDisplayHeight)
                {
                    dragging = 2;
                }

                if (dragging == 0)
                {

                }
                else if (dragging == 1)
                {
                    EditWindow.manager.soundManager.songTime = Mathf.Clamp((e.mousePosition.x * zoom.x - scroll.x) / 100, 0, EditWindow.manager.soundManager.clip.length);
                }
                else if (dragging == 2)
                {
                    scroll.x = -(e.mousePosition.x / window.position.width) * EditWindow.manager.soundManager.waveformTextureWidth;
                }
                break;

            case EventType.MouseUp:
                dragging = -1;
                break;

            case EventType.MouseDrag:
                if (dragging == 0)
                {

                }
                else if (dragging == 1)
                {
                    EditWindow.manager.soundManager.songTime = Mathf.Clamp((e.mousePosition.x * zoom.x - scroll.x) / 100, 0, EditWindow.manager.soundManager.clip.length);
                }
                else if (dragging == 2)
                {
                    scroll.x = -(e.mousePosition.x / window.position.width) * EditWindow.manager.soundManager.waveformTextureWidth;
                }
                break;
        }
    }

    public AudioDisplayManager(EditWindow parentWindow)
    {
        window = EditorWindow.GetWindow<EditWindow>();
    }
}

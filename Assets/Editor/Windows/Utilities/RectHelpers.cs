﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RectHelpers
{
    public static bool OverlappingRect(Rect first, Rect second)
    {
        if (first.x < second.xMax && first.y < second.yMax && first.xMax > second.x && first.yMax > second.y)
        {
            return true;
        }

        return false;
    }

    public static Rect WorldToScreen(Rect rect, Vector2 scroll, Vector2 zoom)
    {
        return new Rect(new Vector2((rect.position.x + scroll.x) * (1 / zoom.x), (rect.position.y + scroll.y) * (1 / zoom.y)), new Vector2(rect.size.x * (1 / zoom.x), rect.size.y * (1 / zoom.y)));
    }

    public static Rect WorldToScreenNoResize(Rect rect, Vector2 scroll, Vector2 zoom)
    {
        return new Rect(new Vector2((rect.position.x + scroll.x) * (1 / zoom.x), (rect.position.y + scroll.y) * (1 / zoom.y)), rect.size);
    }

    public static Vector2 MouseToWorld(Vector2 mousePosition, Vector2 scroll, Vector2 zoom)
    {
        return new Vector2((mousePosition.x * zoom.x - scroll.x) / 100, (mousePosition.y * zoom.y - scroll.y) / 100);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EnemyPlacementTab
{
    EditWindow window;
    EnemyManager manager;

    public void Draw(Vector2 scroll, Vector2 zoom)
    {
        foreach (Enemy enemy in manager.enemies)
        {
            enemy.UpdateDuration();
            Rect rect = enemy.enemyRect;
            rect.position += new Vector2(0, AudioDisplayManager.audioDisplayHeight);
            EditorGUI.DrawRect(RectHelpers.WorldToScreen(rect, scroll, zoom), Color.white);
        }
    }

    public EnemyPlacementTab(EditWindow parentWindow, EnemyManager enemyManager)
    {
        window = EditorWindow.GetWindow<EditWindow>();
        manager = enemyManager;
    }
}

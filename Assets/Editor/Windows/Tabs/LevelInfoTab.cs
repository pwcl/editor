﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class LevelInfoTab
{
    EditWindow window;
    GUIStyle rich = new GUIStyle();
    static float labelWidth = 250;
    AudioClip previousClip;
    LevelInfo comparison;

    public void Draw()
    {
        try
        {
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;
            GUI.skin.label.wordWrap = false;
            rich.richText = true;
            GUILayout.BeginHorizontal();
            GUILayout.Label("Song Clip", GUILayout.Width(labelWidth));
            previousClip = (AudioClip)EditorGUILayout.ObjectField(EditWindow.manager.soundManager.clip, typeof(AudioClip), true, GUILayout.Width(250));
            if (previousClip != EditWindow.manager.GetSong())
            {
                EditWindow.manager.SetSong(previousClip);
            }
            if (EditWindow.manager.GetSong() == null)
            {
                GUILayout.Label("<color=#ff0000ff><< ADD AUDIO HERE</color>", rich);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Song Name", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.songName = GUILayout.TextField(EditWindow.manager.levelInfo.songName, GUILayout.Width(250));
            if (EditWindow.manager.levelInfo.songName == null || EditWindow.manager.levelInfo.songName == "")
            {
                GUILayout.Label("<color=#ff0000ff><< FILL</color>", rich);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Song Artist", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.songArtists = GUILayout.TextField(EditWindow.manager.levelInfo.songArtists, GUILayout.Width(250));
            if (EditWindow.manager.levelInfo.songArtists == null || EditWindow.manager.levelInfo.songArtists == "")
            {
                GUILayout.Label("<color=#ff0000ff><< FILL</color>", rich);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Mapper", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.mapper = GUILayout.TextField(EditWindow.manager.levelInfo.mapper, GUILayout.Width(250));
            if (EditWindow.manager.levelInfo.mapper == null || EditWindow.manager.levelInfo.mapper == "")
            {
                GUILayout.Label("<color=#ff0000ff><< FILL</color>", rich);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Song Length", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.songLength = EditorGUILayout.DelayedFloatField(EditWindow.manager.levelInfo.songLength, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("BPM", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.bpm = EditorGUILayout.DelayedFloatField(EditWindow.manager.levelInfo.bpm, GUILayout.Width(250));
            if (EditWindow.manager.levelInfo.bpm == 0)
            {
                GUILayout.Label("<color=#ff0000ff><< FILL</color>", rich);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Scene Desription", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.description = GUILayout.TextField(EditWindow.manager.levelInfo.description, GUILayout.Width(250));
            if (EditWindow.manager.levelInfo.description == null || EditWindow.manager.levelInfo.description == "")
            {
                GUILayout.Label("<color=#ff0000ff><< FILL</color>", rich);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Enemy Style", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.enemyStyle = (LevelInfo.EnemySets)EditorGUILayout.EnumPopup("", EditWindow.manager.levelInfo.enemyStyle, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Obstacle Style", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.obstacleStyle = (LevelInfo.ObstacleSets)EditorGUILayout.EnumPopup("", EditWindow.manager.levelInfo.obstacleStyle, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Material Style", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.materialStyle = (LevelInfo.MaterialPropertiesSet)EditorGUILayout.EnumPopup("", EditWindow.manager.levelInfo.materialStyle, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Move Speed =D (will break stuff)", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.moveSpeed = EditorGUILayout.DelayedFloatField("", EditWindow.manager.levelInfo.moveSpeed, GUILayout.Width(250));
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            GUILayout.Label("Mapped Maps", GUILayout.Width(labelWidth));

            GUILayout.BeginHorizontal(GUILayout.Width(80));
            GUILayout.Label("Easy", GUILayout.Width(30));
            EditWindow.manager.levelInfo.hasMaps[0] = EditorGUILayout.Toggle("", EditWindow.manager.levelInfo.hasMaps[0], GUILayout.Width(20));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.Width(100));
            GUILayout.Label("Normal", GUILayout.Width(45));
            EditWindow.manager.levelInfo.hasMaps[1] = EditorGUILayout.Toggle("", EditWindow.manager.levelInfo.hasMaps[1], GUILayout.Width(20));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.Width(80));
            GUILayout.Label("Hard", GUILayout.Width(30));
            EditWindow.manager.levelInfo.hasMaps[2] = EditorGUILayout.Toggle("", EditWindow.manager.levelInfo.hasMaps[2], GUILayout.Width(20));
            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            GUILayout.Label("Main Color ", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.mainColor = EditorGUILayout.ColorField(EditWindow.manager.levelInfo.mainColor, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Fog Color ", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.fogColor = EditorGUILayout.ColorField(EditWindow.manager.levelInfo.fogColor, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Glow Color ", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.glowColor = EditorGUILayout.ColorField(EditWindow.manager.levelInfo.glowColor, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Enemy Color ", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.enemyColor = EditorGUILayout.ColorField(EditWindow.manager.levelInfo.enemyColor, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("OnBeat Timing Window Size (in MS)", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.timingWindowSize = EditorGUILayout.DelayedFloatField("", EditWindow.manager.levelInfo.timingWindowSize, GUILayout.Width(250));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Song Preview Time", GUILayout.Width(labelWidth));
            EditWindow.manager.levelInfo.previewTime = EditorGUILayout.DelayedFloatField(EditWindow.manager.levelInfo.previewTime, GUILayout.Width(250));
            if (!EditWindow.manager.IsPlaying() && EditWindow.manager.GetSong() != null)
            {
                if (GUILayout.Button("start preview", GUILayout.Width(100), GUILayout.Height(25)))
                {
                    EditWindow.manager.UpdateSong(EditWindow.manager.levelInfo.previewTime, 0);
                }
            }
            else if (EditWindow.manager.GetSong() != null)
            {
                if (GUILayout.Button("stop preview", GUILayout.Width(100), GUILayout.Height(25)))
                {
                    EditWindow.manager.UpdateSong(0, 1);
                }
            }
            GUILayout.EndHorizontal();

            if (!EditWindow.manager.levelInfo.CheckSame(comparison))
            {
                EditorUtility.SetDirty(EditWindow.manager);
                comparison = EditWindow.manager.levelInfo.Clone();
            }
        }
        catch (ArgumentException e)
        {
            ArgumentException ignore = e;
        }
    }

    public LevelInfoTab(EditWindow parentWindow)
    {
        window = EditorWindow.GetWindow<EditWindow>();
        comparison = EditorManager.Instance.levelInfo.Clone();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LevelColorsTab
{
    EditWindow window;
    LightingManager lightingManager;
    GUIStyle rich = new GUIStyle();
    GenericMenu createMenu;
    Vector2 mousePosition;
    Vector2 scroll;
    Vector2 zoom;
    static float smoothShift = 15;
    int draggingShift = -1;
    int shiftIndex = -1;
    List<Rect> shiftRects = new List<Rect>(0);

    public void Draw(Vector2 scroll, Vector2 zoom)
    {
        float rectHeight = (window.position.height - AudioDisplayManager.audioDisplayHeight - 30) / 4;

        rich.richText = true;
        rich.fontStyle = FontStyle.Bold;
        rich.fontSize = Mathf.FloorToInt(rectHeight * 0.8f);
        rich.padding = new RectOffset(0, 0, 0, 0);
        rich.alignment = TextAnchor.MiddleLeft;
        rich.clipping = TextClipping.Overflow;

        {
            Rect mainRect = new Rect(0, AudioDisplayManager.audioDisplayHeight, EditorManager.Instance.levelInfo.songLength * 100, rectHeight);
            Rect fogRect = new Rect(0, AudioDisplayManager.audioDisplayHeight + rectHeight, EditorManager.Instance.levelInfo.songLength * 100, rectHeight);
            Rect glowRect = new Rect(0, AudioDisplayManager.audioDisplayHeight + rectHeight * 2, EditorManager.Instance.levelInfo.songLength * 100, rectHeight);
            Rect enemyRect = new Rect(0, AudioDisplayManager.audioDisplayHeight + rectHeight * 3, EditorManager.Instance.levelInfo.songLength * 100, rectHeight);

            EditorGUI.DrawRect(RectHelpers.WorldToScreen(mainRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), lightingManager.trackSections[0].mainColor);
            EditorGUI.DrawRect(RectHelpers.WorldToScreen(fogRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), lightingManager.trackSections[0].fogColor);
            EditorGUI.DrawRect(RectHelpers.WorldToScreen(glowRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), lightingManager.trackSections[0].glowColor);
            EditorGUI.DrawRect(RectHelpers.WorldToScreen(enemyRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), lightingManager.trackSections[0].enemyColor);
        }

        Color mainTextColor = lightingManager.trackSections[0].mainColor;
        Color fogTextColor = lightingManager.trackSections[0].fogColor;
        Color glowTextColor = lightingManager.trackSections[0].glowColor;
        Color enemyTextColor = lightingManager.trackSections[0].enemyColor;


        Color lastMain = lightingManager.trackSections[0].mainColor;
        Color lastFog = lightingManager.trackSections[0].fogColor;
        Color lastGlow = lightingManager.trackSections[0].glowColor;
        Color lastEnemy = lightingManager.trackSections[0].enemyColor;

        shiftRects.Clear();

        for (int i = 0; i < lightingManager.colorShifts.Count; i++)
        {
            ColorShift shift = lightingManager.colorShifts[i];

            Rect mainRect = new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight, (EditorManager.Instance.levelInfo.songLength * 100) - (shift.start * 100), rectHeight);
            Rect fogRect = new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight + rectHeight, (EditorManager.Instance.levelInfo.songLength * 100) - (shift.start * 100), rectHeight);
            Rect glowRect = new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight + rectHeight * 2, (EditorManager.Instance.levelInfo.songLength * 100) - (shift.start * 100), rectHeight);
            Rect enemyRect = new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight + rectHeight * 3, (EditorManager.Instance.levelInfo.songLength * 100) - (shift.start * 100), rectHeight);

            EditorGUI.DrawRect(RectHelpers.WorldToScreen(mainRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), shift.colors.mainColor);
            EditorGUI.DrawRect(RectHelpers.WorldToScreen(fogRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), shift.colors.fogColor);
            EditorGUI.DrawRect(RectHelpers.WorldToScreen(glowRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), shift.colors.glowColor);
            EditorGUI.DrawRect(RectHelpers.WorldToScreen(enemyRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), shift.colors.storedEnemyColor);


            Rect mainSelector = RectHelpers.WorldToScreen(new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight, (shift.end - shift.start) * 100, rectHeight), new Vector2(scroll.x, 0), new Vector2(zoom.x, 1));
            Rect fogSelector = RectHelpers.WorldToScreen(new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight + rectHeight, (shift.end - shift.start) * 100, rectHeight), new Vector2(scroll.x, 0), new Vector2(zoom.x, 1));
            Rect glowSelector = RectHelpers.WorldToScreen(new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight + rectHeight * 2, (shift.end - shift.start) * 100, rectHeight), new Vector2(scroll.x, 0), new Vector2(zoom.x, 1));
            Rect enemySelector = RectHelpers.WorldToScreen(new Rect(shift.start * 100, AudioDisplayManager.audioDisplayHeight + rectHeight * 3, (shift.end - shift.start) * 100, rectHeight), new Vector2(scroll.x, 0), new Vector2(zoom.x, 1));

            shift.colors.mainColor = EditorGUI.ColorField(mainSelector, GUIContent.none, shift.colors.mainColor, false, false, false);
            shift.colors.fogColor = EditorGUI.ColorField(fogSelector, GUIContent.none, shift.colors.fogColor, false, false, false);
            shift.colors.glowColor = EditorGUI.ColorField(glowSelector, GUIContent.none, shift.colors.glowColor, false, false, false);
            shift.colors.storedEnemyColor = EditorGUI.ColorField(enemySelector, GUIContent.none, shift.colors.storedEnemyColor, false, false, false);


            float sliceWidth = (shift.end - shift.start) / smoothShift;
            float lastSliceEnd = shift.start * 100;

            for (int j = 0; j < smoothShift; j++)
            {
                Rect mainSlice = new Rect(lastSliceEnd, AudioDisplayManager.audioDisplayHeight, sliceWidth * 100 + 0.1f, rectHeight);
                Rect fogSlice = new Rect(lastSliceEnd, AudioDisplayManager.audioDisplayHeight + rectHeight, sliceWidth * 100 + 0.1f, rectHeight);
                Rect glowSlice = new Rect(lastSliceEnd, AudioDisplayManager.audioDisplayHeight + rectHeight * 2, sliceWidth * 100 + 0.1f, rectHeight);
                Rect enemySlice = new Rect(lastSliceEnd, AudioDisplayManager.audioDisplayHeight + rectHeight * 3, sliceWidth * 100 + 0.1f, rectHeight);

                lastSliceEnd = mainSlice.xMax - 1;

                EditorGUI.DrawRect(RectHelpers.WorldToScreen(mainSlice, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.Lerp(lastMain, shift.colors.mainColor, j / smoothShift));
                EditorGUI.DrawRect(RectHelpers.WorldToScreen(fogSlice, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.Lerp(lastFog, shift.colors.fogColor, j / smoothShift));
                EditorGUI.DrawRect(RectHelpers.WorldToScreen(glowSlice, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.Lerp(lastGlow, shift.colors.glowColor, j / smoothShift));
                EditorGUI.DrawRect(RectHelpers.WorldToScreen(enemySlice, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.Lerp(lastEnemy, shift.colors.storedEnemyColor, j / smoothShift));
            }

            lastMain = shift.colors.mainColor;
            lastFog = shift.colors.fogColor;
            lastGlow = shift.colors.glowColor;
            lastEnemy = shift.colors.storedEnemyColor;


            float barHeight = window.position.height - AudioDisplayManager.audioDisplayHeight - 30;


            Rect barBase = new Rect(shift.start * 100 - 6, AudioDisplayManager.audioDisplayHeight, 6, barHeight);
            Rect barOverly = new Rect(shift.start * 100 - 6, AudioDisplayManager.audioDisplayHeight + barHeight / 4, 6, barHeight / 2);
            Rect barBase2 = new Rect(shift.end * 100, AudioDisplayManager.audioDisplayHeight, 6, barHeight);
            Rect barOverly2 = new Rect(shift.end * 100, AudioDisplayManager.audioDisplayHeight + barHeight / 4, 6, barHeight / 2);

            EditorGUI.DrawRect(RectHelpers.WorldToScreenNoResize(barBase, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.black);
            EditorGUI.DrawRect(RectHelpers.WorldToScreenNoResize(barOverly, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.white);
            EditorGUI.DrawRect(RectHelpers.WorldToScreenNoResize(barBase2, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.black);
            EditorGUI.DrawRect(RectHelpers.WorldToScreenNoResize(barOverly2, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), Color.white);

            EditorGUIUtility.AddCursorRect(RectHelpers.WorldToScreenNoResize(barBase, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), MouseCursor.SlideArrow);
            EditorGUIUtility.AddCursorRect(RectHelpers.WorldToScreenNoResize(barBase2, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)), MouseCursor.SlideArrow);

            shiftRects.Add(RectHelpers.WorldToScreenNoResize(barBase, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)));
            shiftRects.Add(RectHelpers.WorldToScreenNoResize(barBase2, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)));


            if (RectHelpers.WorldToScreen(mainRect, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)).x < 0)
            {
                mainTextColor = shift.colors.mainColor;
                fogTextColor = shift.colors.fogColor;
                glowTextColor = shift.colors.glowColor;
                enemyTextColor = shift.colors.storedEnemyColor;
            }
        }

        Rect mainText = new Rect(0, AudioDisplayManager.audioDisplayHeight, 0, rectHeight);
        Rect fogText = new Rect(0, AudioDisplayManager.audioDisplayHeight + rectHeight, 0, rectHeight);
        Rect glowText = new Rect(0, AudioDisplayManager.audioDisplayHeight + rectHeight * 2, 0, rectHeight);
        Rect enemyText = new Rect(0, AudioDisplayManager.audioDisplayHeight + rectHeight * 3, 0, rectHeight);

        string mainColor = stringconverters.convertC(InvertColor(mainTextColor));
        string fogColor = stringconverters.convertC(InvertColor(fogTextColor));
        string glowColor = stringconverters.convertC(InvertColor(glowTextColor));
        string enemyColor = stringconverters.convertC(InvertColor(enemyTextColor));

        EditorGUI.LabelField(mainText, $"<color=#{mainColor}>Main</color>", rich);
        EditorGUI.LabelField(fogText, $"<color=#{fogColor}>Fog</color>", rich);
        EditorGUI.LabelField(glowText, $"<color=#{glowColor}>Glow</color>", rich);
        EditorGUI.LabelField(enemyText, $"<color=#{enemyColor}>Enemy</color>", rich);
    }

    public void Click(Event e, ref Vector2 scroll, Vector2 zoom)
    {
        mousePosition = e.mousePosition;
        this.scroll = scroll;
        this.zoom = zoom;

        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.mousePosition.y > AudioDisplayManager.audioDisplayHeight)
                {
                    switch (e.button)
                    {
                        case 0:
                            draggingShift = -1;
                            shiftIndex = -1;
                            for (int i = 0; i < shiftRects.Count; i += 2)
                            {
                                if (RectHelpers.OverlappingRect(new Rect(e.mousePosition, Vector2.zero), shiftRects[i])) {
                                    draggingShift = 0;
                                    shiftIndex = Mathf.FloorToInt(i / 2);
                                } else if (RectHelpers.OverlappingRect(new Rect(e.mousePosition, Vector2.zero), shiftRects[i + 1])) {
                                    draggingShift = 1;
                                    shiftIndex = Mathf.FloorToInt(i / 2);
                                }
                            }
                            break;

                        case 1:
                            createMenu.ShowAsContext();
                            break;
                    }
                }
                break;

            case EventType.MouseUp:
                switch (e.button)
                {
                    case 0:
                        draggingShift = -1;
                        shiftIndex = -1;
                        break;
                }
                break;
        }

        if (draggingShift != -1)
        {
            if (draggingShift == 0)
            {
                lightingManager.colorShifts[shiftIndex].start = (e.mousePosition.x * zoom.x - scroll.x) / 100;

                if (shiftIndex == 0)
                {
                    lightingManager.colorShifts[shiftIndex].start = Mathf.Clamp(lightingManager.colorShifts[shiftIndex].start, 0, lightingManager.colorShifts[shiftIndex].end);
                } else
                {
                    lightingManager.colorShifts[shiftIndex].start = Mathf.Clamp(lightingManager.colorShifts[shiftIndex].start, lightingManager.colorShifts[shiftIndex - 1].end, lightingManager.colorShifts[shiftIndex].end);
                }
            } else
            {
                lightingManager.colorShifts[shiftIndex].end = (e.mousePosition.x * zoom.x - scroll.x) / 100;

                if (shiftIndex == lightingManager.colorShifts.Count - 1)
                {
                    lightingManager.colorShifts[shiftIndex].end = Mathf.Clamp(lightingManager.colorShifts[shiftIndex].end, lightingManager.colorShifts[shiftIndex].start, EditorManager.Instance.levelInfo.songLength);
                }
                else
                {
                    lightingManager.colorShifts[shiftIndex].end = Mathf.Clamp(lightingManager.colorShifts[shiftIndex].end, lightingManager.colorShifts[shiftIndex].start, lightingManager.colorShifts[shiftIndex + 1].start);
                }
            }
        }


        if (lightingManager.colorShifts.Count == 0 || (e.mousePosition.x * zoom.x - scroll.x) / 100 < lightingManager.colorShifts[0].start)
        {
            PWInstpector.Instance.currentlyInspecting = PWInstpector.InspectingType.ColorShift;
            PWInstpector.Instance.inspectingShift = -1;
        }

        for (int i = 0; i < shiftRects.Count; i += 2)
        {
            if (RectHelpers.OverlappingRect(new Rect(e.mousePosition, Vector2.zero), new Rect(shiftRects[i].position, new Vector2(shiftRects[i + 1].xMax - shiftRects[i].xMin, shiftRects[i].height))))
            {
                if (EditorWindow.HasOpenInstances<PWInstpector>())
                {
                    PWInstpector.Instance.currentlyInspecting = PWInstpector.InspectingType.ColorShift;
                    PWInstpector.Instance.inspectingShift = Mathf.FloorToInt(i / 2);
                }
            }
        }
    }

    public LevelColorsTab(EditWindow window)
    {
        this.window = EditorWindow.GetWindow<EditWindow>();
        lightingManager = EditorManager.Instance.lightingManager;

        createMenu = new GenericMenu();
        createMenu.AddItem(new GUIContent("Add shift"), false, new GenericMenu.MenuFunction(CreateShift));
        createMenu.AddItem(new GUIContent("Remove shift"), false, new GenericMenu.MenuFunction(RemoveShift));
    }

    void CreateShift()
    {
        float mTime = RectHelpers.MouseToWorld(mousePosition, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)).x;
        bool overlappingShift = false;

        foreach (ColorShift shift in lightingManager.colorShifts)
        {
            if (mTime > shift.start && mTime < shift.end)
            {
                overlappingShift = true;
            }
            if (mTime + 1 > shift.start && mTime + 1 < shift.end)
            {
                overlappingShift = true;
            }
        }

        if (!overlappingShift) {
            lightingManager.InsertShift(new ColorShift(mTime, mTime + 1, new ColorData(Random.ColorHSV(), Random.ColorHSV(), Random.ColorHSV(), Random.ColorHSV())));
        }
    }

    void RemoveShift()
    {
        float mTime = RectHelpers.MouseToWorld(mousePosition, new Vector2(scroll.x, 0), new Vector2(zoom.x, 1)).x;

        for (int i = 0; i < lightingManager.colorShifts.Count; i++)
        {
            ColorShift shift = lightingManager.colorShifts[i];

            if (mTime > shift.start && mTime < shift.end)
            {
                lightingManager.colorShifts.RemoveAt(i);
                return;
            }
            if (mTime + 1 > shift.start && mTime + 1 < shift.end)
            {
                lightingManager.colorShifts.RemoveAt(i);
                return;
            }
        }
    }

    Color InvertColor(Color col)
    {
        float h = 0;
        float s = 0;
        float v = 0;
        Color.RGBToHSV(col, out h, out s, out v);
        h = (h * 360 + 180) % 360 / 360;
        s = (s * 100 + 50) % 100 / 100;
        v = (v * 100 + 50) % 100 / 100;
        return Color.HSVToRGB(h, s, v);
    }
}

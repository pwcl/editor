﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExporter
{
    public static float version = 0.5f;

    public async static void ExportLevel()
    {
        var jsonSettings = new JsonSerializerSettings
        {
            Converters =
            {
                new ColorConverter(),
                new VectorConverter(),
                new QuaternionConverter(),
                new StringEnumConverter()
            }
        };

        GeoSet geoset = new GeoSet();
        //pwevent events = new pwevent();
        //pwbeat beats = new pwbeat();

        EditorManager.Instance.levelInfo.version = version;
        geoset.version = version;

        GameObject[] sceneObjects = EditorSceneManager.GetActiveScene().GetRootGameObjects();
        bool firstUntagged = true;
        GameObject selected = Selection.activeGameObject;

        foreach (GameObject sceneObject in sceneObjects)
        {
            if (sceneObject.tag == "Untagged")
            {
                if (firstUntagged)
                {
                    EditorUtility.DisplayDialog("Untagged gameobjects detected!", "There are one or more untagged gameobjects in your scene! You'll need to decide whether they are a part of the geoset or a prop", "Ok");
                    firstUntagged = false;
                }

                Selection.activeGameObject = sceneObject;
                SceneView.lastActiveSceneView.LookAt(sceneObject.transform.position);
                await Task.Delay(500);
                int type = EditorUtility.DisplayDialogComplex("Untagged gameobjects detected!", $"Gameobject: {sceneObject.name}\nPosition: {sceneObject.transform.position}", "GeoSet", "Cancel Export", "Prop");
                //EditorWindow.GetWindow<SceneView>().camera.transform.position = sceneObject.transform.position;
                //EditorWindow.GetWindow<SceneView>().camera.transform.Translate(Vector3.back * 5 * sceneObject.transform.localScale.magnitude, Space.Self);

                switch (type)
                {
                    case 0:
                        sceneObject.tag = "GeoSet";
                        break;

                    case 2:
                        sceneObject.tag = "Prop";
                        break;

                    case 1:
                        break;
                }
            }
        }

        // GEOSET
        GameObject GeosetObject = new GameObject();
        GeosetObject.tag = "Editor Object";
        GeosetObject.name = "Geoset combining object";
        GeosetObject.AddComponent<MeshFilter>();
        GeosetObject.AddComponent<MeshRenderer>();
        Mesh allmesh = new Mesh();
        List<MeshFilter> filters = new List<MeshFilter>(0);
        foreach (GameObject ob in GameObject.FindGameObjectsWithTag("GeoSet"))
        {
            if (ob.GetComponent<MeshFilter>() != null)
            {
                filters.Add(ob.GetComponent<MeshFilter>());
            }
            filters.AddRange(GetChildMeshFilters(ob.transform));
        }
        CombineInstance[] combineInstance = new CombineInstance[filters.Count];
        for (int i = 0; i < filters.Count; i++)
        {
            combineInstance[i].mesh = filters[i].sharedMesh;
            combineInstance[i].transform = filters[i].gameObject.transform.localToWorldMatrix;
        }
        GeosetObject.GetComponent<MeshFilter>().sharedMesh = new Mesh();
        GeosetObject.GetComponent<MeshFilter>().sharedMesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        GeosetObject.GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combineInstance);
        GeosetObject.GetComponent<MeshFilter>().sharedMesh.RecalculateNormals();
        GeosetObject.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
        SlicerSlice[] zSlices = MeshSlice.SliceZ(GeosetObject.GetComponent<MeshFilter>().sharedMesh, Vector3Int.zero);
        for (int i = 0; i < zSlices.Length; i++)
        {
            zSlices[i].id = new Vector3Int(zSlices[i].id.x, zSlices[i].id.y, zSlices[i].id.z);
            List<Vector3> adjustedverts = new List<Vector3>(0);
            foreach (Vector3 vert in zSlices[i].verts)
            {
                float xadj = zSlices[i].id.x;
                float yadj = zSlices[i].id.y;
                float zadj = zSlices[i].id.z;
                Vector3 newvec = new Vector3(vert.x - xadj * 16, vert.y - yadj * 16, vert.z - zadj * 16);
                adjustedverts.Add(newvec);
            }
            zSlices[i].verts = adjustedverts;
            geoset.chunks.Add(zSlices[i]);
        }

        geoset.chunksForSlices.Add(new ChunkSlice());
        geoset.chunksForSlices[0].id = geoset.chunks[0].id.z;
        //EditorUtility.DisplayProgressBar("Exporting Geoset", "Making Chunk Slices", 0.975f);
        Dictionary<int, int> indexID = new Dictionary<int, int>(0);
        for (int i = 0; i < geoset.chunks.Count; i++)
        {
            int thisID = geoset.chunks[i].id.z;
            //Debug.Log(thisID);
            if (!indexID.ContainsKey(thisID))
            {
                ChunkSlice chunks = new ChunkSlice();
                chunks.id = thisID;
                indexID.Add(thisID, geoset.chunksForSlices.Count);
                //Debug.Log($"added {thisID} at {newGeoset.chunksForSlices.Count}");
                geoset.chunksForSlices.Add(chunks);
            }
            //Debug.Log($"chunkslices {indexID[thisID]}");
            geoset.chunksForSlices[indexID[thisID]].chunks.Add(i);
        }

        for (int i = geoset.chunksForSlices.Count - 1; i >= 0; i--)
        {
            if (geoset.chunksForSlices[i].chunks.Count == 0)
            {
                geoset.chunksForSlices.RemoveAt(i);
            }
        }

        EditorUtility.ClearProgressBar();
        GameObject.DestroyImmediate(GeosetObject);

        Directory.CreateDirectory("./Test");
        File.WriteAllText("./Test/level.pw", JsonConvert.SerializeObject(LevelFile.Generate(EditorManager.Instance.levelInfo, version), jsonSettings));
        File.WriteAllText("./Test/Easy.pw_geo", JsonConvert.SerializeObject(geoset, jsonSettings));
        Debug.Log("Export Finished");

        /*public static BeatFile Generate(List<EnemyTrack> enemyTracks, List<ObstacleObject> obstacles, float ver)
        {
        BeatFile beatFile = new BeatFile();
        beatFile.version = ver;

        foreach (EnemyTrack track in enemyTracks)
        {
            List<EnemyDraggable> enemies = track.enemies;
            foreach (EnemyDraggable enemy in enemies)
            {
                Beat beat = new Beat();
                beat.time = enemy.time;

                TargetData target = new TargetData();
                switch (enemy.enemyStyle)
                {
                    case 0:
                        target.type = enemy.set1Type.ToString();
                        break;

                    case 1:
                        target.type = enemy.set2Type.ToString();
                        break;

                    case 2:
                        target.type = enemy.set3Type.ToString();
                        break;
                }
                switch ((int)enemy.distance)
                {
                    case 4:
                        target.distance = TargetData.Distance.Near;
                        break;

                    case 12:
                        target.distance = TargetData.Distance.Middle;
                        break;

                    case 24:
                        target.distance = TargetData.Distance.Far;
                        break;
                }
                target.placement = enemy.placement;
                target.enemyOffset = enemy.offset;
                target.bonusEnemy = enemy.bonusEnemy;
                target.shielded = enemy.shielded;
                target.noGround = enemy.noGround;
                target.noCarve = enemy.noCarve;
                target.playerActionLerp = enemy.playerActionLerp;

                Vector3 lastActionDestination = Vector3.zero;
                foreach (Action act in enemy.actions)
                {
                    EnemyAction action = new EnemyAction(act.actionType);

                    switch (act.actionType)
                    {
                        case Action.ActionType.Wait:
                            action.properties.duration = act.duration;
                            break;

                        case Action.ActionType.Move:
                            action.properties = new EnemyActionMove(new WorldPoint(act.destination.position - lastActionDestination, act.destination.rotation), act.isOverrideAnimationSpeed, act.animationSpeedOverride, enemy.facing);
                            lastActionDestination = act.destination.position;
                            action.properties.duration = act.duration;
                            break;

                        case Action.ActionType.AimStop:
                            action.properties = new EnemyActionAimStop(act.stopFacingOnExit, act.stopLookingOnExit);
                            break;

                        case Action.ActionType.AimAndFire:
                            action.properties = new EnemyActionAimAndFire(act.fireTime);
                            action.properties.duration = act.duration;
                            break;
                    }

                    target.enemySequence.Add(action);
                }

                beatFile.beatTimes.Add(enemy.time);
                beat.targets.Add(target);
                beatFile.beatData.Add(beat);
            }
        }

        foreach (ObstacleObject obstacle in obstacles)
        {
            if (obstacle.time >= 1)
            {
                Beat beat = new Beat();
                beat.time = obstacle.time;

                ObstacleData data = new ObstacleData();
                data.placement = (ObstacleData.ObstaclePlacement)obstacle.currentPlacement;
                data.type = (ObstacleData.ObstacleType)obstacle.currentType;

                beat.obstacles.Add(data);

                beatFile.beatTimes.Add(obstacle.time);
                beatFile.beatData.Add(beat);
            }
        }

        return beatFile;
        }
        */
    }

    public static MeshFilter[] GetChildMeshFilters(Transform parentObject)
    {
        List<MeshFilter> meshFilters = new List<MeshFilter>(0);

        for (int i = 0; i < parentObject.childCount; i++)
        {
            if (parentObject.GetChild(i).gameObject.GetComponent<MeshFilter>() == null)
            {
                foreach (MeshFilter filter in GetChildMeshFilters(parentObject.GetChild(i)))
                {
                    meshFilters.Add(filter);
                }
            }
            else
            {
                meshFilters.Add(parentObject.GetChild(i).gameObject.GetComponent<MeshFilter>());
                foreach (MeshFilter filter in GetChildMeshFilters(parentObject.GetChild(i)))
                {
                    meshFilters.Add(filter);
                }
            }
        }

        return meshFilters.ToArray();
    }
}

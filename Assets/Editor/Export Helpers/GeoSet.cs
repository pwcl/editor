﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using Newtonsoft.Json;

public class GeoSet
{
    public float version;
    public List<ChunkSlice> chunksForSlices = new List<ChunkSlice>(0);
    public List<SlicerSlice> chunks = new List<SlicerSlice>(0);
    public List<DecoratorCube> decCubes = new List<DecoratorCube>(0);
    public List<StaticProp> staticProps = new List<StaticProp>(0);
    public List<DynamProp> dynamicProps = new List<DynamProp>(0);
    public List<string> spropNames = new List<string>(0);
    public List<string> dpropNames = new List<string>(0);
}

public class Chunk
{
    public Vector3Int id;
    public int[] tris;
    public Vector3[] verts;
}

public class ChunkSlice
{
    public List<int> chunks = new List<int>(0);
    public int id;
}

[JsonObject(MemberSerialization.OptIn)]
public class StaticProp
{
    public GameObject prop;
    [JsonProperty]
    public Vector3 pos;
    [JsonProperty]
    public Vector3 rot;
    [JsonProperty]
    public Vector3 scale;
    [JsonProperty]
    public string name;
    [JsonProperty]
    public string path;
}

public class DynamProp
{
    public Vector3 pos;
    public Vector3 rot;
    public Vector3 scale;
    public Vector3 Start;
    public Vector3 End;
    public string name;
}
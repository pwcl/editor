﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System;
using static UnityEditor.EditorGUILayout;

[ExecuteInEditMode]
public class TestScript : EditorWindow
{
    static EditorWindow window;

    [MenuItem("Balls/Balls")]
    static void Balls()
    {
        //EditorUtility.DisplayProgressBar("Balls", "Balls", 0.69f);
    }

    [MenuItem("Balls/No Balls")]
    static void NoBalls()
    {
        EditorUtility.ClearProgressBar();
    }

    [MenuItem("Balls/Thonk %e")]
    static void Thonk()
    {
        LevelExporter.ExportLevel();
    }

    [MenuItem("Balls/Window")]
    static void Init()
    {
        window = GetWindow<TestScript>();
        window.Show();

        for (int i = 0; i < 10000; i++)
        {
            rects.Add(new Rect(UnityEngine.Random.Range(-2500, 2500), UnityEngine.Random.Range(-2500, 2500), UnityEngine.Random.Range(1, 100), UnityEngine.Random.Range(1, 100)));
            colors.Add(UnityEngine.Random.ColorHSV());
        }
    }

    private void OnValidate()
    {
        for (int i = 0; i < 10000; i++)
        {
            rects.Add(new Rect(UnityEngine.Random.Range(-2500, 2500), UnityEngine.Random.Range(-2500, 2500), UnityEngine.Random.Range(1, 100), UnityEngine.Random.Range(1, 100)));
            colors.Add(UnityEngine.Random.ColorHSV());
        }
    }

    static List<Rect> rects = new List<Rect>(0);
    static List<Color> colors = new List<Color>(0);
    static Vector2 scroll = Vector2.zero;
    static Vector2 zoom = Vector2.one;
    Vector2 mouseo = Vector2.zero;
    Vector2 mousen = Vector2.zero;
    Vector2 mousen2 = Vector2.zero;

    void OnGUI()
    {
        Event e = Event.current;

        switch (e.type)
        {
            case EventType.ScrollWheel:
                float mouseo;
                float mousen;
                float offset;
                if (e.alt && e.control)
                {
                    mouseo = (e.mousePosition.y) * (zoom.y / 1);

                    zoom.y += e.delta.y / 30;
                    zoom.y = Mathf.Clamp(zoom.y, 0.1f, 10);

                    mousen = (e.mousePosition.y) * (zoom.y / 1);

                    offset = (mousen - mouseo);
                    scroll.y += offset;
                    //Debug.Log("zoomx: " + zoom);
                }
                else if (e.alt)
                {
                    mouseo = (e.mousePosition.x) * (zoom.x / 1);

                    zoom.x += e.delta.y / 30;
                    zoom.x = Mathf.Clamp(zoom.x, 0.1f, 10);

                    mousen = (e.mousePosition.x) * (zoom.x / 1);

                    offset = (mousen - mouseo);
                    scroll.x += offset;
                    //Debug.Log("zoomx: " + zoom);
                }
                else if (!e.control)
                {
                    scroll.x -= (e.delta.y / 3) * (Convert.ToInt32(e.shift) * 4 + 1) * (zoom.x + 1);
                    //Debug.Log("scroll.x: " + scroll.x);
                }
                else
                {
                    scroll.y -= (e.delta.y / 3) * (zoom.y / 1) * 4 * (Convert.ToInt32(e.shift) * 8 + 1);
                    //Debug.Log("scroll.y: " + scroll.y);
                }
                break;
        }


        DrawRects();
        Repaint();
    }

    void DrawRects()
    {
        for (int i = 0; i < 10000; i++)
        {
            Rect rect = rects[i];
            Rect drawRect = RectHelpers.WorldToScreen(rect, scroll, zoom);

            if (RectHelpers.OverlappingRect(drawRect, position))
            {
                EditorGUI.DrawRect(drawRect, colors[i]);
            }
        }
    }

    bool CullRect(Rect rect)
    {
        if (rect.x < position.width && rect.y < position.height && rect.x + rect.width > 0 && rect.y + rect.height > 0)
        {
            return true;
        }

        return false;
    }
}

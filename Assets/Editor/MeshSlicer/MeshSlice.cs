﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using System.IO;

public class MeshSlice
{
    public MeshSlice()
    {

    }

    public static SlicerSlice[] SliceZ(Mesh mesh, Vector3Int oldIndex)
    {
        List<SlicerSlice> slicerSlices = new List<SlicerSlice>(0);
        Dictionary<int, SlicerSlice> sliceDictionary = new Dictionary<int, SlicerSlice>(0);

        //Debug.Log(mesh.bounds.ClosestPoint(new Vector3(0, 0, -500)));
        //Debug.Log(mesh.bounds.ClosestPoint(new Vector3(0, 0, 5000)));
        for (int i = Mathf.FloorToInt(mesh.bounds.ClosestPoint(mesh.bounds.center + new Vector3(0, 0, -50000000)).z / 16); i <= Mathf.CeilToInt(mesh.bounds.ClosestPoint(mesh.bounds.center + new Vector3(0, 0, 50000000)).z / 16); i++)
        {
            SlicerSlice slice = new SlicerSlice { id = new Vector3Int(0, 0, i), normalIndex = i };
            slice.id += oldIndex;
            slicerSlices.Add(slice);
            sliceDictionary.Add(i, slice);
            //Debug.Log(i);
        }
        //Debug.Log(slicerSlices.Count);
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        Vector3[] oldVerts = mesh.vertices;
        Vector2[] oldUvs = mesh.uv;
        int[] oldTris = mesh.triangles;
        //Debug.Log(oldVerts.Length);
        //Debug.Log(oldTris.Length);
        //Debug.Log(oldTris.Length/3);

        /*foreach (int slice in sliceDictionary.Keys)
        {
            Debug.Log("Z " + slice);
        }*/



        for (int i = 0; i < oldTris.Length; i += 3)
        {
            //EditorUtility.DisplayProgressBar("Exporting Geoset", "Slicing Z axis", 0.69f);
            int tri1Z = Mathf.FloorToInt(oldVerts[oldTris[i]].z / 16);
            int tri2Z = Mathf.FloorToInt(oldVerts[oldTris[i + 1]].z / 16);
            int tri3Z = Mathf.FloorToInt(oldVerts[oldTris[i + 2]].z / 16);

            if (!sliceDictionary.ContainsKey(tri1Z))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(0, 0, tri1Z), normalIndex = tri1Z };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri1Z, newSlice);
            }
            if (!sliceDictionary.ContainsKey(tri2Z))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(0, 0, tri2Z), normalIndex = tri2Z };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri2Z, newSlice);
            }
            if (!sliceDictionary.ContainsKey(tri3Z))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(0, 0, tri3Z), normalIndex = tri3Z };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri3Z, newSlice);
            }

            if (tri1Z == tri2Z && tri2Z == tri3Z)
            {
                //Debug.Log("Z key " + tri1Z);
                SlicerSlice slice = sliceDictionary[tri1Z];
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i]))
                {
                    slice.verts.Add(oldVerts[oldTris[i]]);
                    slice.uvs.Add(oldUvs[oldTris[i]]);
                    slice.oldToNewVerts.Add(oldTris[i], slice.verts.Count - 1);
                }
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i + 1]))
                {
                    slice.verts.Add(oldVerts[oldTris[i + 1]]);
                    slice.uvs.Add(oldUvs[oldTris[i + 1]]);
                    slice.oldToNewVerts.Add(oldTris[i + 1], slice.verts.Count - 1);
                }
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i + 2]))
                {
                    slice.verts.Add(oldVerts[oldTris[i + 2]]);
                    slice.uvs.Add(oldUvs[oldTris[i + 2]]);
                    slice.oldToNewVerts.Add(oldTris[i + 2], slice.verts.Count - 1);
                }
                slice.tris.Add(slice.oldToNewVerts[oldTris[i]]);
                slice.tris.Add(slice.oldToNewVerts[oldTris[i + 1]]);
                slice.tris.Add(slice.oldToNewVerts[oldTris[i + 2]]);
            }
            else
            {
                int maxZ = 0;
                int minZ = 0;
                List<int> VertOrder = new List<int>(3) { i, i + 1, i + 2 };
                maxZ = tri1Z;
                if (tri2Z > maxZ)
                {
                    maxZ = tri2Z;
                    VertOrder = new List<int>(3) { i + 1, i + 2, i };
                }
                else if (tri3Z > maxZ)
                {
                    maxZ = tri3Z;
                    VertOrder = new List<int>(3) { i + 2, i, i + 1 };
                }
                minZ = tri1Z;
                if (tri2Z < minZ)
                {
                    minZ = tri2Z;
                }
                else if (tri3Z < minZ)
                {
                    minZ = tri3Z;
                }


                if (!sliceDictionary[tri1Z].oldToNewVerts.ContainsKey(oldTris[i]))
                {
                    sliceDictionary[tri1Z].verts.Add(oldVerts[oldTris[i]]);
                    sliceDictionary[tri1Z].uvs.Add(oldUvs[oldTris[i]]);
                    sliceDictionary[tri1Z].oldToNewVerts.Add(oldTris[i], sliceDictionary[tri1Z].verts.Count - 1);
                }
                if (!sliceDictionary[tri2Z].oldToNewVerts.ContainsKey(oldTris[i + 1]))
                {
                    sliceDictionary[tri2Z].verts.Add(oldVerts[oldTris[i + 1]]);
                    sliceDictionary[tri2Z].uvs.Add(oldUvs[oldTris[i + 1]]);
                    sliceDictionary[tri2Z].oldToNewVerts.Add(oldTris[i + 1], sliceDictionary[tri2Z].verts.Count - 1);
                }
                if (!sliceDictionary[tri3Z].oldToNewVerts.ContainsKey(oldTris[i + 2]))
                {
                    sliceDictionary[tri3Z].verts.Add(oldVerts[oldTris[i + 2]]);
                    sliceDictionary[tri3Z].uvs.Add(oldUvs[oldTris[i + 2]]);
                    sliceDictionary[tri3Z].oldToNewVerts.Add(oldTris[i + 2], sliceDictionary[tri3Z].verts.Count - 1);
                }

                List<SlicedVert> lastVerts = new List<SlicedVert>(0);
                List<SlicedVert> lastUvs = new List<SlicedVert>(0);
                //Debug.Log(minZ + ", " + maxZ);
                for (int j = maxZ; j >= minZ; j--)
                {
                    SlicerSlice slice = sliceDictionary[j];
                    Vector3 ZeroOne = new Vector3(-99999, -99999, -99999);
                    bool ZeroOnePerpendicular = false;
                    Vector3 ZeroTwo = new Vector3(-99999, -99999, -99999);
                    bool ZeroTwoPerpendicular = false;
                    Vector3 OneTwo = new Vector3(-99999, -99999, -99999);
                    bool OneTwoPerpendicular = false;

                    List<SlicedVert> sectionsVerts = new List<SlicedVert>(0);
                    List<SlicedVert> sectionsUvs = new List<SlicedVert>(0);

                    foreach (SlicedVert vert in lastVerts)
                    {
                        sectionsVerts.Add(vert);
                    }
                    foreach (SlicedVert vert in lastUvs)
                    {
                        sectionsUvs.Add(vert);
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[0]]].z / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[0]]], oldTris[VertOrder[0]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[0]]], oldTris[VertOrder[0]]));
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].z / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[1]]], oldTris[VertOrder[1]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[1]]], oldTris[VertOrder[1]]));
                    }

                    if (j != minZ)
                    {
                        if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].z / 16) >= j && Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].z / 16) < j)
                        {
                            OneTwoPerpendicular = LinePlaneIntersection(out OneTwo, oldVerts[oldTris[VertOrder[1]]], (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[1]]]).normalized, new Vector3(0, 0, 1), new Vector3(0, 0, j * 16));
                            sectionsVerts.Add(new SlicedVert(OneTwo, -1));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -1));
                            //Debug.Log("OneTwo: " + oldVerts[oldTris[VertOrder[2]]] + "-" + oldVerts[oldTris[VertOrder[1]]] + "=" + (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[1]]]).normalized);
                            //Debug.Log("OneTwo: " + OneTwo);
                        }
                        else
                        {
                            ZeroOnePerpendicular = LinePlaneIntersection(out ZeroOne, oldVerts[oldTris[VertOrder[0]]], (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[0]]]).normalized, new Vector3(0, 0, 1), new Vector3(0, 0, j * 16));
                            sectionsVerts.Add(new SlicedVert(ZeroOne, -2));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -2));
                            //Debug.Log("ZeroOne: " + oldVerts[oldTris[VertOrder[1]]] + "-" + oldVerts[oldTris[VertOrder[0]]] + "=" + (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[0]]]).normalized);
                            //Debug.Log("ZeroOne: " + ZeroOne);
                        }

                        if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].z / 16) >= j && Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].z / 16) < j)
                        {
                            OneTwoPerpendicular = LinePlaneIntersection(out OneTwo, oldVerts[oldTris[VertOrder[2]]], (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[2]]]).normalized, new Vector3(0, 0, 1), new Vector3(0, 0, j * 16));
                            sectionsVerts.Add(new SlicedVert(OneTwo, -3));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -3));
                            //Debug.Log("OneTwo: " + oldVerts[oldTris[VertOrder[1]]] + "-" + oldVerts[oldTris[VertOrder[2]]] + "=" + (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[2]]]).normalized);
                            //Debug.Log("OneTwo: " + OneTwo);
                        }
                        else
                        {
                            ZeroTwoPerpendicular = LinePlaneIntersection(out ZeroTwo, oldVerts[oldTris[VertOrder[0]]], (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[0]]]).normalized, new Vector3(0, 0, 1), new Vector3(0, 0, j * 16));
                            sectionsVerts.Add(new SlicedVert(ZeroTwo, -4));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -4));
                            //Debug.Log("ZeroTwo: " + oldVerts[oldTris[VertOrder[2]]] + "-" + oldVerts[oldTris[VertOrder[0]]] + "=" + (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[0]]]).normalized);
                            //Debug.Log("ZeroTwo: " + ZeroTwo);

                        }
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].z / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[2]]], oldTris[VertOrder[2]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[2]]], oldTris[VertOrder[2]]));
                    }


                    /*foreach (SlicedVert ver in sectionsVerts) {
                        Debug.Log(ver.vert + ", " + ver.indexed);
                    }*/



                    for (int k = 0; k < sectionsVerts.Count - 2; k++)
                    {
                        /*slice.verts.Add(sectionsVerts[0].vert);
                        slice.uvs.Add(sectionsUvs[0].vert);
                        slice.tris.Add(slice.verts.Count - 1);
                        slice.verts.Add(sectionsVerts[1 + k].vert);
                        slice.uvs.Add(sectionsUvs[1+k].vert);
                        slice.tris.Add(slice.verts.Count - 1);
                        slice.verts.Add(sectionsVerts[2 + k].vert);
                        slice.uvs.Add(sectionsUvs[2 + k].vert);
                        slice.tris.Add(slice.verts.Count - 1);*/

                        //Debug.Log(sectionsVerts[0].vert + " to " + sectionsVerts[1+k].vert + " to " + sectionsVerts[2+k].vert);
                        if (sectionsVerts[0].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[0].vert))
                            {
                                slice.verts.Add(sectionsVerts[0].vert);
                                slice.uvs.Add(sectionsUvs[0].vert);
                                slice.createdVerts.Add(sectionsVerts[0].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[0].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[0].indexed]);
                        }

                        if (sectionsVerts[1 + k].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[1 + k].vert))
                            {
                                slice.verts.Add(sectionsVerts[1 + k].vert);
                                slice.uvs.Add(sectionsUvs[1 + k].vert);
                                slice.createdVerts.Add(sectionsVerts[1 + k].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[1 + k].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[1 + k].indexed]);
                        }

                        if (sectionsVerts[2 + k].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[2 + k].vert))
                            {
                                slice.verts.Add(sectionsVerts[2 + k].vert);
                                slice.uvs.Add(sectionsUvs[2 + k].vert);
                                slice.createdVerts.Add(sectionsVerts[2 + k].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[2 + k].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[2 + k].indexed]);
                        }
                    }

                    lastVerts.Clear();
                    lastUvs.Clear();
                    if (ZeroTwoPerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(ZeroTwo, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], ZeroTwo);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                    if (OneTwoPerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(OneTwo, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                    if (ZeroOnePerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(ZeroOne, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], ZeroOne);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                }
            }
        }

        foreach (SlicerSlice slice in slicerSlices)
        {
            slice.mesh = new Mesh();
            slice.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            slice.mesh.vertices = slice.verts.ToArray();
            slice.mesh.triangles = slice.tris.ToArray();
            slice.mesh.uv = slice.uvs.ToArray();
        }

        return slicerSlices.ToArray();
    }


    public static SlicerSlice[] SliceY(Mesh mesh, Vector3Int oldIndex)
    {
        List<SlicerSlice> slicerSlices = new List<SlicerSlice>(0);
        Dictionary<int, SlicerSlice> sliceDictionary = new Dictionary<int, SlicerSlice>(0);

        //Debug.Log(mesh.bounds.ClosestPoint(new Vector3(0, 0, -500)));
        //Debug.Log(mesh.bounds.ClosestPoint(new Vector3(0, 0, 5000)));
        for (int i = Mathf.FloorToInt(mesh.bounds.ClosestPoint(mesh.bounds.center + new Vector3(0, -50000000, 0)).y / 16); i <= Mathf.CeilToInt(mesh.bounds.ClosestPoint(mesh.bounds.center + new Vector3(0, 50000000000000, 0)).y / 16); i++)
        {
            SlicerSlice slice = new SlicerSlice { id = new Vector3Int(0, i, 0), normalIndex = i };
            slice.id += oldIndex;
            slicerSlices.Add(slice);
            sliceDictionary.Add(i, slice);
            //Debug.Log(i);
        }
        //Debug.Log(slicerSlices.Count);

        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        Vector3[] oldVerts = mesh.vertices;
        Vector2[] oldUvs = mesh.uv;
        int[] oldTris = mesh.triangles;
        //Debug.Log(oldVerts.Length);
        //Debug.Log(oldTris.Length);
        /*for (int i = 0; i < oldVerts.Length; i++) {
            SlicerSlice slice = sliceDictionary[16*Mathf.FloorToInt(oldVerts[i].z/16)];
            slice.verts.Add(oldVerts[i]);
            slice.oldToNewVerts.Add(i, slice.verts.Count-1);
        }*/
        //Debug.Log(oldTris.Length/3);

        /*foreach (int slice in sliceDictionary.Keys)
        {
            Debug.Log("Y " + slice);
        }*/

        for (int i = 0; i < oldTris.Length; i += 3)
        {
            //EditorUtility.DisplayProgressBar("Exporting Geoset", "Slicing Y axis", i / oldTris.Length);
            int tri1Y = Mathf.FloorToInt(oldVerts[oldTris[i]].y / 16);
            int tri2Y = Mathf.FloorToInt(oldVerts[oldTris[i + 1]].y / 16);
            int tri3Y = Mathf.FloorToInt(oldVerts[oldTris[i + 2]].y / 16);

            if (!sliceDictionary.ContainsKey(tri1Y))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(0, tri1Y, 0), normalIndex = tri1Y };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri1Y, newSlice);
            }
            if (!sliceDictionary.ContainsKey(tri2Y))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(0, tri2Y, 0), normalIndex = tri2Y };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri2Y, newSlice);
            }
            if (!sliceDictionary.ContainsKey(tri3Y))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(0, tri3Y, 0), normalIndex = tri3Y };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri3Y, newSlice);
            }

            if (tri1Y == tri2Y && tri1Y == tri3Y)
            {
                //Debug.Log("Y key " + tri1Y);
                SlicerSlice slice = sliceDictionary[tri1Y];
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i]))
                {
                    slice.verts.Add(oldVerts[oldTris[i]]);
                    slice.uvs.Add(oldUvs[oldTris[i]]);
                    slice.oldToNewVerts.Add(oldTris[i], slice.verts.Count - 1);
                }
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i + 1]))
                {
                    slice.verts.Add(oldVerts[oldTris[i + 1]]);
                    slice.uvs.Add(oldUvs[oldTris[i + 1]]);
                    slice.oldToNewVerts.Add(oldTris[i + 1], slice.verts.Count - 1);
                }
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i + 2]))
                {
                    slice.verts.Add(oldVerts[oldTris[i + 2]]);
                    slice.uvs.Add(oldUvs[oldTris[i + 2]]);
                    slice.oldToNewVerts.Add(oldTris[i + 2], slice.verts.Count - 1);
                }
                slice.tris.Add(slice.oldToNewVerts[oldTris[i]]);
                slice.tris.Add(slice.oldToNewVerts[oldTris[i + 1]]);
                slice.tris.Add(slice.oldToNewVerts[oldTris[i + 2]]);
            }
            else
            {
                int maxY = 0;
                int minY = 0;
                List<int> VertOrder = new List<int>(3) { i, i + 1, i + 2 };
                maxY = tri1Y;
                if (tri2Y > maxY)
                {
                    maxY = tri2Y;
                    VertOrder = new List<int>(3) { i + 1, i + 2, i };
                }
                else if (tri3Y > maxY)
                {
                    maxY = tri3Y;
                    VertOrder = new List<int>(3) { i + 2, i, i + 1 };
                }
                minY = tri1Y;
                if (tri2Y < minY)
                {
                    minY = tri2Y;
                }
                else if (tri3Y < minY)
                {
                    minY = tri3Y;
                }


                if (!sliceDictionary[tri1Y].oldToNewVerts.ContainsKey(oldTris[i]))
                {
                    sliceDictionary[tri1Y].verts.Add(oldVerts[oldTris[i]]);
                    sliceDictionary[tri1Y].uvs.Add(oldUvs[oldTris[i]]);
                    sliceDictionary[tri1Y].oldToNewVerts.Add(oldTris[i], sliceDictionary[tri1Y].verts.Count - 1);
                }
                if (!sliceDictionary[tri2Y].oldToNewVerts.ContainsKey(oldTris[i + 1]))
                {
                    sliceDictionary[tri2Y].verts.Add(oldVerts[oldTris[i + 1]]);
                    sliceDictionary[tri2Y].uvs.Add(oldUvs[oldTris[i + 1]]);
                    sliceDictionary[tri2Y].oldToNewVerts.Add(oldTris[i + 1], sliceDictionary[tri2Y].verts.Count - 1);
                }
                if (!sliceDictionary[tri3Y].oldToNewVerts.ContainsKey(oldTris[i + 2]))
                {
                    sliceDictionary[tri3Y].verts.Add(oldVerts[oldTris[i + 2]]);
                    sliceDictionary[tri3Y].uvs.Add(oldUvs[oldTris[i + 2]]);
                    sliceDictionary[tri3Y].oldToNewVerts.Add(oldTris[i + 2], sliceDictionary[tri3Y].verts.Count - 1);
                }

                List<SlicedVert> lastVerts = new List<SlicedVert>(0);
                List<SlicedVert> lastUvs = new List<SlicedVert>(0);
                //Debug.Log(minY + ", " + maxY);
                for (int j = maxY; j >= minY; j--)
                {
                    SlicerSlice slice = sliceDictionary[j];
                    Vector3 ZeroOne = new Vector3(-99999, -99999, -99999);
                    bool ZeroOnePerpendicular = false;
                    Vector3 ZeroTwo = new Vector3(-99999, -99999, -99999);
                    bool ZeroTwoPerpendicular = false;
                    Vector3 OneTwo = new Vector3(-99999, -99999, -99999);
                    bool OneTwoPerpendicular = false;

                    List<SlicedVert> sectionsVerts = new List<SlicedVert>(0);
                    List<SlicedVert> sectionsUvs = new List<SlicedVert>(0);

                    foreach (SlicedVert vert in lastVerts)
                    {
                        sectionsVerts.Add(vert);
                    }
                    foreach (SlicedVert vert in lastUvs)
                    {
                        sectionsUvs.Add(vert);
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[0]]].y / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[0]]], oldTris[VertOrder[0]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[0]]], oldTris[VertOrder[0]]));
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].y / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[1]]], oldTris[VertOrder[1]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[1]]], oldTris[VertOrder[1]]));
                    }

                    if (j != minY)
                    {
                        if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].y / 16) >= j && Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].y / 16) < j)
                        {
                            OneTwoPerpendicular = LinePlaneIntersection(out OneTwo, oldVerts[oldTris[VertOrder[1]]], (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[1]]]).normalized, new Vector3(0, 1, 0), new Vector3(0, j * 16, 0));
                            sectionsVerts.Add(new SlicedVert(OneTwo, -1));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -1));
                            //Debug.Log("OneTwo: " + oldVerts[oldTris[VertOrder[2]]] + "-" + oldVerts[oldTris[VertOrder[1]]] + "=" + (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[1]]]).normalized);
                            //Debug.Log("OneTwo: " + OneTwo);
                        }
                        else
                        {
                            ZeroOnePerpendicular = LinePlaneIntersection(out ZeroOne, oldVerts[oldTris[VertOrder[0]]], (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[0]]]).normalized, new Vector3(0, 1, 0), new Vector3(0, j * 16, 0));
                            sectionsVerts.Add(new SlicedVert(ZeroOne, -2));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -2));
                            //Debug.Log("ZeroOne: " + oldVerts[oldTris[VertOrder[1]]] + "-" + oldVerts[oldTris[VertOrder[0]]] + "=" + (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[0]]]).normalized);
                            //Debug.Log("ZeroOne: " + ZeroOne);
                        }

                        if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].y / 16) >= j && Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].y / 16) < j)
                        {
                            OneTwoPerpendicular = LinePlaneIntersection(out OneTwo, oldVerts[oldTris[VertOrder[2]]], (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[2]]]).normalized, new Vector3(0, 1, 0), new Vector3(0, j * 16, 0));
                            sectionsVerts.Add(new SlicedVert(OneTwo, -3));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -3));
                            //Debug.Log("OneTwo: " + oldVerts[oldTris[VertOrder[1]]] + "-" + oldVerts[oldTris[VertOrder[2]]] + "=" + (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[2]]]).normalized);
                            //Debug.Log("OneTwo: " + OneTwo);
                        }
                        else
                        {
                            ZeroTwoPerpendicular = LinePlaneIntersection(out ZeroTwo, oldVerts[oldTris[VertOrder[0]]], (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[0]]]).normalized, new Vector3(0, 1, 0), new Vector3(0, j * 16, 0));
                            sectionsVerts.Add(new SlicedVert(ZeroTwo, -4));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -4));
                            //Debug.Log("ZeroTwo: " + oldVerts[oldTris[VertOrder[2]]] + "-" + oldVerts[oldTris[VertOrder[0]]] + "=" + (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[0]]]).normalized);
                            //Debug.Log("ZeroTwo: " + ZeroTwo);
                        }
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].y / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[2]]], oldTris[VertOrder[2]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[2]]], oldTris[VertOrder[2]]));
                    }

                    //Debug.Log("sc " + sectionsVerts.Count);
                    /*foreach (SlicedVert ver in sectionsVerts) {
                        Debug.Log(ver.vert + ", " + ver.indexed);
                    }*/

                    for (int k = 0; k < sectionsVerts.Count - 2; k++)
                    {

                        slice.verts.Add(sectionsVerts[0].vert);
                        slice.uvs.Add(sectionsUvs[0].vert);
                        slice.tris.Add(slice.verts.Count - 1);
                        slice.verts.Add(sectionsVerts[1 + k].vert);
                        slice.uvs.Add(sectionsUvs[1 + k].vert);
                        slice.tris.Add(slice.verts.Count - 1);
                        slice.verts.Add(sectionsVerts[2 + k].vert);
                        slice.uvs.Add(sectionsUvs[2 + k].vert);
                        slice.tris.Add(slice.verts.Count - 1);

                        //Debug.Log(sectionsVerts[0].vert + " to " + sectionsVerts[1+k].vert + " to " + sectionsVerts[2+k].vert);
                        /*if (sectionsVerts[0].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[0].vert))
                            {
                                slice.verts.Add(sectionsVerts[0].vert);
                                slice.uvs.Add(sectionsUvs[0].vert);
                                slice.createdVerts.Add(sectionsVerts[0].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[0].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[0].indexed]);
                        }

                        if (sectionsVerts[1 + k].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[1 + k].vert))
                            {
                                slice.verts.Add(sectionsVerts[1 + k].vert);
                                slice.uvs.Add(sectionsUvs[1 + k].vert);
                                slice.createdVerts.Add(sectionsVerts[1 + k].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[1 + k].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[1 + k].indexed]);
                        }

                        if (sectionsVerts[2 + k].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[2 + k].vert))
                            {
                                slice.verts.Add(sectionsVerts[2 + k].vert);
                                slice.uvs.Add(sectionsUvs[2 + k].vert);
                                slice.createdVerts.Add(sectionsVerts[2 + k].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[2 + k].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[2 + k].indexed]);
                        }*/
                    }

                    lastVerts.Clear();
                    lastUvs.Clear();
                    if (ZeroTwoPerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(ZeroTwo, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], ZeroTwo);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                    if (OneTwoPerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(OneTwo, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                    if (ZeroOnePerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(ZeroOne, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], ZeroOne);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                }
            }

        }

        foreach (SlicerSlice slice in slicerSlices)
        {
            slice.mesh = new Mesh();
            slice.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            slice.mesh.vertices = slice.verts.ToArray();
            slice.mesh.triangles = slice.tris.ToArray();
            slice.mesh.uv = slice.uvs.ToArray();
        }

        return slicerSlices.ToArray();
    }


    public static SlicerSlice[] SliceX(Mesh mesh, Vector3Int oldIndex)
    {
        List<SlicerSlice> slicerSlices = new List<SlicerSlice>(0);
        Dictionary<int, SlicerSlice> sliceDictionary = new Dictionary<int, SlicerSlice>(0);

        //Debug.Log(mesh.bounds.ClosestPoint(new Vector3(0, 0, -500)));
        //Debug.Log(mesh.bounds.ClosestPoint(new Vector3(0, 0, 5000)));
        for (int i = Mathf.FloorToInt(mesh.bounds.ClosestPoint(mesh.bounds.center + new Vector3(-500000000000000, 0, 0)).x / 16); i <= Mathf.FloorToInt(mesh.bounds.ClosestPoint(mesh.bounds.center + new Vector3(500000000000000, 0, 0)).x / 16); i++)
        {
            SlicerSlice slice = new SlicerSlice { id = new Vector3Int(i, 0, 0), normalIndex = i };
            slice.id += oldIndex;
            slicerSlices.Add(slice);
            sliceDictionary.Add(i, slice);
            //Debug.Log(i);
        }
        //Debug.Log(slicerSlices.Count);

        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        Vector3[] oldVerts = mesh.vertices;
        Vector2[] oldUvs = mesh.uv;
        int[] oldTris = mesh.triangles;
        //Debug.Log(oldVerts.Length);
        //Debug.Log(oldTris.Length);
        /*for (int i = 0; i < oldVerts.Length; i++) {
            SlicerSlice slice = sliceDictionary[16*Mathf.FloorToInt(oldVerts[i].z/16)];
            slice.verts.Add(oldVerts[i]);
            slice.oldToNewVerts.Add(i, slice.verts.Count-1);
        }*/
        //Debug.Log(oldTris.Length/3);

        /*(foreach (int slice in sliceDictionary.Keys)
        {
            Debug.Log("X " + slice);
        }*/

        for (int i = 0; i < oldTris.Length; i += 3)
        {
            //EditorUtility.DisplayProgressBar("Exporting Geoset", "Slicing X axis", i / oldTris.Length);
            int tri1X = Mathf.FloorToInt(oldVerts[oldTris[i]].x / 16);
            int tri2X = Mathf.FloorToInt(oldVerts[oldTris[i + 1]].x / 16);
            int tri3X = Mathf.FloorToInt(oldVerts[oldTris[i + 2]].x / 16);

            if (!sliceDictionary.ContainsKey(tri1X))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(tri1X, 0, 0), normalIndex = tri1X };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri1X, newSlice);
            }
            if (!sliceDictionary.ContainsKey(tri2X))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(tri2X, 0, 0), normalIndex = tri2X };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri2X, newSlice);
            }
            if (!sliceDictionary.ContainsKey(tri3X))
            {
                SlicerSlice newSlice = new SlicerSlice { id = new Vector3Int(tri3X, 0, 0), normalIndex = tri3X };
                newSlice.id += oldIndex;
                slicerSlices.Add(newSlice);
                sliceDictionary.Add(tri3X, newSlice);
            }

            if (tri1X == tri2X && tri1X == tri3X)
            {
                //Debug.Log("X key " + tri1X);
                SlicerSlice slice = sliceDictionary[tri1X];
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i]))
                {
                    slice.verts.Add(oldVerts[oldTris[i]]);
                    slice.uvs.Add(oldUvs[oldTris[i]]);
                    slice.oldToNewVerts.Add(oldTris[i], slice.verts.Count - 1);
                }
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i + 1]))
                {
                    slice.verts.Add(oldVerts[oldTris[i + 1]]);
                    slice.uvs.Add(oldUvs[oldTris[i + 1]]);
                    slice.oldToNewVerts.Add(oldTris[i + 1], slice.verts.Count - 1);
                }
                if (!slice.oldToNewVerts.ContainsKey(oldTris[i + 2]))
                {
                    slice.verts.Add(oldVerts[oldTris[i + 2]]);
                    slice.uvs.Add(oldUvs[oldTris[i + 2]]);
                    slice.oldToNewVerts.Add(oldTris[i + 2], slice.verts.Count - 1);
                }
                slice.tris.Add(slice.oldToNewVerts[oldTris[i]]);
                slice.tris.Add(slice.oldToNewVerts[oldTris[i + 1]]);
                slice.tris.Add(slice.oldToNewVerts[oldTris[i + 2]]);
                /*slice.verts.Add(oldVerts[oldTris[i]]);
                slice.uvs.Add(oldUvs[oldTris[i]]);
                slice.tris.Add(slice.verts.Count - 1);
                slice.verts.Add(oldVerts[oldTris[i+1]]);
                slice.uvs.Add(oldUvs[oldTris[i+1]]);
                slice.tris.Add(slice.verts.Count - 1);
                slice.verts.Add(oldVerts[oldTris[i+2]]);
                slice.uvs.Add(oldUvs[oldTris[i+2]]);
                slice.tris.Add(slice.verts.Count - 1);*/
            }
            else
            {
                int maxX = 0;
                int minX = 0;
                List<int> VertOrder = new List<int>(3) { i, i + 1, i + 2 };
                maxX = tri1X;
                if (tri2X > maxX)
                {
                    maxX = tri2X;
                    VertOrder = new List<int>(3) { i + 1, i + 2, i };
                }
                else if (tri3X > maxX)
                {
                    maxX = tri3X;
                    VertOrder = new List<int>(3) { i + 2, i, i + 1 };
                }
                minX = tri1X;
                if (tri2X < minX)
                {
                    minX = tri2X;
                }
                else if (tri3X < minX)
                {
                    minX = tri3X;
                }


                if (!sliceDictionary[tri1X].oldToNewVerts.ContainsKey(oldTris[i]))
                {
                    sliceDictionary[tri1X].verts.Add(oldVerts[oldTris[i]]);
                    sliceDictionary[tri1X].uvs.Add(oldUvs[oldTris[i]]);
                    sliceDictionary[tri1X].oldToNewVerts.Add(oldTris[i], sliceDictionary[tri1X].verts.Count - 1);
                }
                if (!sliceDictionary[tri2X].oldToNewVerts.ContainsKey(oldTris[i + 1]))
                {
                    sliceDictionary[tri2X].verts.Add(oldVerts[oldTris[i + 1]]);
                    sliceDictionary[tri2X].uvs.Add(oldUvs[oldTris[i + 1]]);
                    sliceDictionary[tri2X].oldToNewVerts.Add(oldTris[i + 1], sliceDictionary[tri2X].verts.Count - 1);
                }
                if (!sliceDictionary[tri3X].oldToNewVerts.ContainsKey(oldTris[i + 2]))
                {
                    sliceDictionary[tri3X].verts.Add(oldVerts[oldTris[i + 2]]);
                    sliceDictionary[tri3X].uvs.Add(oldUvs[oldTris[i + 2]]);
                    sliceDictionary[tri3X].oldToNewVerts.Add(oldTris[i + 2], sliceDictionary[tri3X].verts.Count - 1);
                }

                List<SlicedVert> lastVerts = new List<SlicedVert>(0);
                List<SlicedVert> lastUvs = new List<SlicedVert>(0);
                //Debug.Log(minX + ", " + maxX);
                for (int j = maxX; j >= minX; j--)
                {
                    SlicerSlice slice = sliceDictionary[j];
                    Vector3 ZeroOne = new Vector3(-99999, -99999, -99999);
                    bool ZeroOnePerpendicular = false;
                    Vector3 ZeroTwo = new Vector3(-99999, -99999, -99999);
                    bool ZeroTwoPerpendicular = false;
                    Vector3 OneTwo = new Vector3(-99999, -99999, -99999);
                    bool OneTwoPerpendicular = false;

                    List<SlicedVert> sectionsVerts = new List<SlicedVert>(0);
                    List<SlicedVert> sectionsUvs = new List<SlicedVert>(0);

                    foreach (SlicedVert vert in lastVerts)
                    {
                        sectionsVerts.Add(vert);
                    }
                    foreach (SlicedVert vert in lastUvs)
                    {
                        sectionsUvs.Add(vert);
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[0]]].x / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[0]]], oldTris[VertOrder[0]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[0]]], oldTris[VertOrder[0]]));
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].x / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[1]]], oldTris[VertOrder[1]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[1]]], oldTris[VertOrder[1]]));
                    }

                    if (j != minX)
                    {
                        if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].x / 16) >= j && Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].x / 16) < j)
                        {
                            OneTwoPerpendicular = LinePlaneIntersection(out OneTwo, oldVerts[oldTris[VertOrder[1]]], (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[1]]]).normalized, new Vector3(1, 0, 0), new Vector3(j * 16, 0, 0));
                            sectionsVerts.Add(new SlicedVert(OneTwo, -1));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -1));
                            //Debug.Log("OneTwo: " + oldVerts[oldTris[VertOrder[2]]] + "-" + oldVerts[oldTris[VertOrder[1]]] + "=" + (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[1]]]).normalized);
                            //Debug.Log("OneTwo: " + OneTwo);
                        }
                        else
                        {
                            ZeroOnePerpendicular = LinePlaneIntersection(out ZeroOne, oldVerts[oldTris[VertOrder[0]]], (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[0]]]).normalized, new Vector3(1, 0, 0), new Vector3(j * 16, 0, 0));
                            sectionsVerts.Add(new SlicedVert(ZeroOne, -2));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -2));
                            //Debug.Log("ZeroOne: " + oldVerts[oldTris[VertOrder[1]]] + "-" + oldVerts[oldTris[VertOrder[0]]] + "=" + (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[0]]]).normalized);
                            //Debug.Log("ZeroOne: " + ZeroOne);
                        }

                        if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].x / 16) >= j && Mathf.FloorToInt(oldVerts[oldTris[VertOrder[1]]].x / 16) < j)
                        {
                            OneTwoPerpendicular = LinePlaneIntersection(out OneTwo, oldVerts[oldTris[VertOrder[2]]], (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[2]]]).normalized, new Vector3(1, 0, 0), new Vector3(j * 16, 0, 0));
                            sectionsVerts.Add(new SlicedVert(OneTwo, -3));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -3));
                            //Debug.Log("OneTwo: " + oldVerts[oldTris[VertOrder[1]]] + "-" + oldVerts[oldTris[VertOrder[2]]] + "=" + (oldVerts[oldTris[VertOrder[1]]] - oldVerts[oldTris[VertOrder[2]]]).normalized);
                            //Debug.Log("OneTwo: " + OneTwo);
                        }
                        else
                        {
                            ZeroTwoPerpendicular = LinePlaneIntersection(out ZeroTwo, oldVerts[oldTris[VertOrder[0]]], (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[0]]]).normalized, new Vector3(1, 0, 0), new Vector3(j * 16, 0, 0));
                            sectionsVerts.Add(new SlicedVert(ZeroTwo, -4));

                            Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                            Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                            sectionsUvs.Add(new SlicedVert(uv_D, -4));
                            //Debug.Log("ZeroTwo: " + oldVerts[oldTris[VertOrder[2]]] + "-" + oldVerts[oldTris[VertOrder[0]]] + "=" + (oldVerts[oldTris[VertOrder[2]]] - oldVerts[oldTris[VertOrder[0]]]).normalized);
                            //Debug.Log("ZeroTwo: " + ZeroTwo);
                        }
                    }

                    if (Mathf.FloorToInt(oldVerts[oldTris[VertOrder[2]]].x / 16) == j)
                    {
                        sectionsVerts.Add(new SlicedVert(oldVerts[oldTris[VertOrder[2]]], oldTris[VertOrder[2]]));
                        sectionsUvs.Add(new SlicedVert(oldUvs[oldTris[VertOrder[2]]], oldTris[VertOrder[2]]));
                    }

                    //Debug.Log("sc " + sectionsVerts.Count);
                    /*foreach (SlicedVert ver in sectionsVerts) {
                        Debug.Log(ver.vert + ", " + ver.indexed);
                    }*/

                    for (int k = 0; k < sectionsVerts.Count - 2; k++)
                    {
                        slice.verts.Add(sectionsVerts[0].vert);
                        slice.uvs.Add(sectionsUvs[0].vert);
                        slice.tris.Add(slice.verts.Count - 1);
                        slice.verts.Add(sectionsVerts[1 + k].vert);
                        slice.uvs.Add(sectionsUvs[1 + k].vert);
                        slice.tris.Add(slice.verts.Count - 1);
                        slice.verts.Add(sectionsVerts[2 + k].vert);
                        slice.uvs.Add(sectionsUvs[2 + k].vert);
                        slice.tris.Add(slice.verts.Count - 1);

                        //Debug.Log(sectionsVerts[0].vert + " to " + sectionsVerts[1+k].vert + " to " + sectionsVerts[2+k].vert);
                        /*if (sectionsVerts[0].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[0].vert))
                            {
                                slice.verts.Add(sectionsVerts[0].vert);
                                slice.uvs.Add(sectionsUvs[0].vert);
                                slice.createdVerts.Add(sectionsVerts[0].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[0].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[0].indexed]);
                        }

                        if (sectionsVerts[1 + k].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[1 + k].vert))
                            {
                                slice.verts.Add(sectionsVerts[1 + k].vert);
                                slice.uvs.Add(sectionsUvs[1 + k].vert);
                                slice.createdVerts.Add(sectionsVerts[1 + k].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[1 + k].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[1 + k].indexed]);
                        }

                        if (sectionsVerts[2 + k].indexed < 0)
                        {
                            if (!slice.createdVerts.ContainsKey(sectionsVerts[2 + k].vert))
                            {
                                slice.verts.Add(sectionsVerts[2 + k].vert);
                                slice.uvs.Add(sectionsUvs[2 + k].vert);
                                slice.createdVerts.Add(sectionsVerts[2 + k].vert, slice.verts.Count - 1);
                            }
                            slice.tris.Add(slice.createdVerts[sectionsVerts[2 + k].vert]);
                        }
                        else
                        {
                            slice.tris.Add(slice.oldToNewVerts[sectionsVerts[2 + k].indexed]);
                        }*/
                    }

                    lastVerts.Clear();
                    lastUvs.Clear();
                    if (ZeroTwoPerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(ZeroTwo, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], ZeroTwo);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                    if (OneTwoPerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(OneTwo, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], OneTwo);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                    if (ZeroOnePerpendicular)
                    {
                        lastVerts.Add(new SlicedVert(ZeroOne, -1));

                        Barycentric b = new Barycentric(oldVerts[oldTris[VertOrder[0]]], oldVerts[oldTris[VertOrder[1]]], oldVerts[oldTris[VertOrder[2]]], ZeroOne);
                        Vector2 uv_D = b.Interpolate(oldUvs[oldTris[VertOrder[0]]], oldUvs[oldTris[VertOrder[1]]], oldUvs[oldTris[VertOrder[2]]]);
                        lastUvs.Add(new SlicedVert(uv_D, -1));
                    }
                }
            }
        }

        foreach (SlicerSlice slice in slicerSlices)
        {
            slice.mesh = new Mesh();
            slice.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
            slice.mesh.vertices = slice.verts.ToArray();
            slice.mesh.triangles = slice.tris.ToArray();
            slice.mesh.uv = slice.uvs.ToArray();
        }

        return slicerSlices.ToArray();
    }



    // Taken from http://wiki.unity3d.com/index.php/3d_Math_functions
    public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 linePoint, Vector3 lineVec, Vector3 planeNormal, Vector3 planePoint)
    {

        float length;
        float dotNumerator;
        float dotDenominator;
        Vector3 vector;
        intersection = Vector3.zero;

        //calculate the distance between the linePoint and the line-plane intersection point
        dotNumerator = Vector3.Dot((planePoint - linePoint), planeNormal);
        dotDenominator = Vector3.Dot(lineVec, planeNormal);

        //line and plane are not parallel
        if (dotDenominator != 0.0f)
        {
            length = dotNumerator / dotDenominator;

            //create a vector from the linePoint to the intersection point
            vector = lineVec.normalized * length;

            //get the coordinates of the line-plane intersection point
            intersection = linePoint + vector;

            return true;
        }

        //output not valid
        else
        {
            return false;
        }
    }
    //create a vector of direction "vector" with length "size"
    public static Vector3 SetVectorLength(Vector3 vector, float size)
    {

        //normalize the vector
        Vector3 vectorNormalized = Vector3.Normalize(vector);

        //scale the vector
        return vectorNormalized *= size;
    }
}

[JsonObject(MemberSerialization.OptIn)]
public class SlicerSlice
{
    [JsonProperty]
    public List<int> tris = new List<int>(0);
    [JsonProperty]
    public List<Vector3> verts = new List<Vector3>(0);
    public List<Vector2> uvs = new List<Vector2>(0);
    public Dictionary<int, int> oldToNewVerts = new Dictionary<int, int>(0);
    public Dictionary<Vector3, int> createdVerts = new Dictionary<Vector3, int>(0);
    [JsonProperty]
    public Vector3Int id;
    public int normalIndex;
    public GameObject gameObject;
    public MeshRenderer renderer;
    public MeshFilter filter;
    public Mesh mesh;

    /*public SlicerSlice(Vector3Int index, int index2) {
        id += index;
        normalIndex = index2;
        gameObject = new GameObject("" + index);
        filter = gameObject.AddComponent<MeshFilter>();
        renderer = gameObject.AddComponent<MeshRenderer>();
        renderer.material = mat;
    }*/

}

class SlicedVert
{

    public Vector3 vert;
    public int indexed = -1;
    public SlicedVert(Vector3 vertice, int index)
    {
        vert = vertice;
        indexed = index;
    }
}
